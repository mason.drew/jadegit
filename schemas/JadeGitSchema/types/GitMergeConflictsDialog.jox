<GUIClass name="GitMergeConflictsDialog" id="fcc38468-c8a9-471c-a8aa-05ec742698ef">
    <superclass name="GitDialog"/>
    <transient>true</transient>
    <subclassTransientAllowed>true</subclassTransientAllowed>
    <transientAllowed>true</transientAllowed>
    <ImplicitInverseRef name="btnBack" id="39cd3943-f2fa-44ea-acdb-15281656bd3b">
        <embedded>true</embedded>
        <type name="Button"/>
    </ImplicitInverseRef>
    <ImplicitInverseRef name="btnNext" id="c68ade03-a9d4-4bb1-9d18-cb08b4516ab8">
        <embedded>true</embedded>
        <type name="Button"/>
    </ImplicitInverseRef>
    <ImplicitInverseRef name="conflict" id="a1072463-4dec-4b03-b55f-859d466020fe">
        <embedded>true</embedded>
        <type name="GitMergeConflict"/>
        <access>protected</access>
    </ImplicitInverseRef>
    <ImplicitInverseRef name="conflicts" id="2a4d61ee-cf7b-485c-b02e-d87d72a6fb94">
        <embedded>true</embedded>
        <type name="GitMergeConflictArray"/>
        <access>protected</access>
    </ImplicitInverseRef>
    <ImplicitInverseRef name="lblCount" id="5463b5d1-9eef-4acb-aaf4-0bc1ad98b28b">
        <embedded>true</embedded>
        <type name="Label"/>
    </ImplicitInverseRef>
    <ImplicitInverseRef name="txtBase" id="8e892fe5-65aa-4205-9e42-27d493f67289">
        <embedded>true</embedded>
        <type name="JadeTextEdit"/>
    </ImplicitInverseRef>
    <ImplicitInverseRef name="txtMerged" id="d449d94c-5dac-4e92-b70f-ad5cc631f496">
        <embedded>true</embedded>
        <type name="JadeTextEdit"/>
    </ImplicitInverseRef>
    <ImplicitInverseRef name="txtOurs" id="34f017e4-c48c-4c2c-8bf6-e2732a439922">
        <embedded>true</embedded>
        <type name="JadeTextEdit"/>
    </ImplicitInverseRef>
    <ImplicitInverseRef name="txtTheirs" id="4b3bfc0d-50fe-4fb5-a48a-f8543a58a314">
        <embedded>true</embedded>
        <type name="JadeTextEdit"/>
    </ImplicitInverseRef>
    <ImplicitInverseRef name="updated" id="95e1be75-f1bf-4453-9824-1e87289cf4f6">
        <memberTypeInverse>true</memberTypeInverse>
        <type name="GitMergeConflictArray"/>
        <access>protected</access>
    </ImplicitInverseRef>
    <JadeMethod name="btnBack_click" id="ac1e847e-4c02-4584-af99-83b4824dc7bc">
        <controlMethod name="Button::click"/>
        <methodInvocation>event</methodInvocation>
        <updating>true</updating>
        <source>btnBack_click(btn: Button input) updating;

begin
	update;
	repopulate(conflicts[ conflicts.indexOf( conflict ) - 1 ]);
end;
</source>
        <Parameter name="btn">
            <usage>input</usage>
            <type name="Button"/>
        </Parameter>
    </JadeMethod>
    <JadeMethod name="btnBack_enable" id="47835d92-fd36-46b2-b5b1-a4f0a9ef833e">
        <controlMethod name="Control::enable"/>
        <methodInvocation>event</methodInvocation>
        <source>btnBack_enable(cntrl: Control input) protected;

begin
	cntrl.enabled := conflict &lt;&gt; conflicts.first();
end;
</source>
        <access>protected</access>
        <Parameter name="cntrl">
            <usage>input</usage>
            <type name="Control"/>
        </Parameter>
    </JadeMethod>
    <JadeMethod name="btnNext_click" id="770af479-b231-40c3-86f4-1d3ea024e215">
        <controlMethod name="Button::click"/>
        <methodInvocation>event</methodInvocation>
        <updating>true</updating>
        <source>btnNext_click(btn: Button input) updating;

begin
	update;
	repopulate(conflicts[ conflicts.indexOf( conflict ) + 1 ]);
end;
</source>
        <Parameter name="btn">
            <usage>input</usage>
            <type name="Button"/>
        </Parameter>
    </JadeMethod>
    <JadeMethod name="btnNext_enable" id="823cd14f-740e-4eec-9242-504b8d8f429d">
        <controlMethod name="Control::enable"/>
        <methodInvocation>event</methodInvocation>
        <source>btnNext_enable(cntrl: Control input) protected;

begin
	cntrl.enabled := conflict &lt;&gt; conflicts.last();
end;
</source>
        <access>protected</access>
        <Parameter name="cntrl">
            <usage>input</usage>
            <type name="Control"/>
        </Parameter>
    </JadeMethod>
    <JadeMethod name="doOK" id="ef302c97-d731-4b25-a80f-1780b7687088">
        <updating>true</updating>
        <source>doOK(): Boolean updating, protected;

vars

begin
	update;
	
	return true;
end;
</source>
        <access>protected</access>
        <ReturnType>
            <type name="Boolean"/>
        </ReturnType>
    </JadeMethod>
    <JadeMethod name="isOK" id="e923324f-5caf-459f-aa58-4038ebb4d870">
        <source>isOK(): Boolean protected;

begin
	if conflicts.size = updated.size then
		return true;
	endif;
	
	if conflicts.size - 1 = updated.size and not updated.includes( conflict ) then
		return txtMerged.text &lt;&gt; conflict.merged;
	endif;
end;
</source>
        <access>protected</access>
        <ReturnType>
            <type name="Boolean"/>
        </ReturnType>
    </JadeMethod>
    <JadeMethod name="populate" id="f6da3209-63a5-4684-bb53-586f243ee2df">
        <updating>true</updating>
        <source>populate(object: Object): Boolean updating, protected;

vars
	
begin
	if object.isKindOf(GitMergeConflictArray) then
		conflicts := object.GitMergeConflictArray;
		conflict := conflicts.first;
	else
		conflict := object.GitMergeConflict;
	endif;

	caption := getPersistentObject.Form.caption &amp; ": " &amp; conflict.path;
	
	txtBase.text := conflict.base;
	txtOurs.text := conflict.ours;
	txtTheirs.text := conflict.theirs;
	
	if conflict.updated &lt;&gt; null then
		txtMerged.text := conflict.updated;
	else
		txtMerged.text := conflict.merged;
	endif;
		
	self.conflict := conflict;
	
	lblCount.caption := conflicts.indexOf( conflict ).String &amp; " of " &amp; conflicts.size.String &amp; " conflicts";
	
	return true;
end;
</source>
        <access>protected</access>
        <Parameter name="object">
            <type name="Object"/>
        </Parameter>
        <ReturnType>
            <type name="Boolean"/>
        </ReturnType>
    </JadeMethod>
    <JadeMethod name="txtMerged_firstChange" id="00b65d0d-8136-4107-8420-83d708611a2f">
        <controlMethod name="JadeTextEdit::firstChange"/>
        <methodInvocation>event</methodInvocation>
        <updating>true</updating>
        <source>txtMerged_firstChange(textedit: JadeTextEdit input) updating;

vars

begin
	enableControls();
end;
</source>
        <Parameter name="textedit">
            <usage>input</usage>
            <type name="JadeTextEdit"/>
        </Parameter>
    </JadeMethod>
    <JadeMethod name="update" id="5991d1e0-38e0-4bf7-b9e1-b0f87ab633d5">
        <source>update() protected;

vars

begin
	if txtMerged.text &lt;&gt; conflict.merged then
		conflict.updated := txtMerged.text;
		if not updated.includes( conflict ) then
			updated.add( conflict );
		endif;
	endif;
end;
</source>
        <access>protected</access>
    </JadeMethod>
</GUIClass>

<GUIClass name="GitSetupSchemaDialog" id="ec017b06-5270-4a9b-bec2-a14399a995a5">
    <superclass name="GitDialog"/>
    <transient>true</transient>
    <subclassTransientAllowed>true</subclassTransientAllowed>
    <transientAllowed>true</transientAllowed>
    <ImplicitInverseRef name="cmbRepository" id="4be56be0-6f99-4cff-a2f7-640d5fb893f7">
        <embedded>true</embedded>
        <type name="ComboBox"/>
    </ImplicitInverseRef>
    <ImplicitInverseRef name="optExclude" id="b1fc8723-fbd5-440c-9e42-c0ba56f5dadc">
        <embedded>true</embedded>
        <type name="OptionButton"/>
    </ImplicitInverseRef>
    <ImplicitInverseRef name="optInclude" id="dfcaa156-8a34-42bd-aded-e487e46662a4">
        <embedded>true</embedded>
        <type name="OptionButton"/>
    </ImplicitInverseRef>
    <PrimAttribute name="schemaName" id="9a0e2279-cf0f-48e6-ad22-cea0ada4ed7e">
        <length>101</length>
        <embedded>true</embedded>
        <type name="String"/>
        <access>protected</access>
    </PrimAttribute>
    <JadeMethod name="cmbRepository_click" id="8dbfa26e-6ea4-4751-afcf-d15b39c70d9b">
        <controlMethod name="ComboBox::click"/>
        <methodInvocation>event</methodInvocation>
        <updating>true</updating>
        <source>cmbRepository_click(combobox: ComboBox input) updating;

begin
	enableControls;
end;
</source>
        <Parameter name="combobox">
            <usage>input</usage>
            <type name="ComboBox"/>
        </Parameter>
    </JadeMethod>
    <JadeMethod name="cmbRepository_displayEntry" id="00c85d6f-685e-47de-bf50-a16500e07a52">
        <controlMethod name="ComboBox::displayEntry"/>
        <methodInvocation>event</methodInvocation>
        <updating>true</updating>
        <source>cmbRepository_displayEntry(combobox: ComboBox input; repo: RepositoryData; lstIndex: Integer):String updating;

begin
	if repo.isActive() then
		return repo.name;
	endif;
end;
</source>
        <Parameter name="combobox">
            <usage>input</usage>
            <type name="ComboBox"/>
        </Parameter>
        <Parameter name="repo">
            <type name="RepositoryData"/>
        </Parameter>
        <Parameter name="lstIndex">
            <type name="Integer"/>
        </Parameter>
        <ReturnType>
            <type name="String"/>
        </ReturnType>
    </JadeMethod>
    <JadeMethod name="cmbRepository_populate" id="c2cfb846-24c8-49fc-807d-75c0df6c9f7d">
        <controlMethod name="Control::populate"/>
        <methodInvocation>event</methodInvocation>
        <updating>true</updating>
        <source>cmbRepository_populate(cntrl: ComboBox input; object: Object):Boolean updating, protected;

begin
	// List repositories available
	cntrl.listCollection(Root@get().repositories, true, null);
	return true;
end;
</source>
        <access>protected</access>
        <Parameter name="cntrl">
            <usage>input</usage>
            <type name="ComboBox"/>
        </Parameter>
        <Parameter name="object">
            <type name="Object"/>
        </Parameter>
        <ReturnType>
            <type name="Boolean"/>
        </ReturnType>
    </JadeMethod>
    <JadeMethod name="doOK" id="3ea49a64-b746-43a2-9814-cf5e1858169a">
        <updating>true</updating>
        <source>doOK(): Boolean updating, protected;

vars
	repo : RepositoryData;

begin
	if optInclude.value then
		repo := cmbRepository.listObject.RepositoryData;
	endif;
	
	beginTransaction;
	GitSchema@init(schemaName, repo);
	commitTransaction;

	return true;
end;
</source>
        <access>protected</access>
        <ReturnType>
            <type name="Boolean"/>
        </ReturnType>
    </JadeMethod>
    <JadeMethod name="isOK" id="a7d8fad4-22b3-4526-a83f-17ea04a96655">
        <source>isOK(): Boolean protected;

begin
	return optExclude.value or
		   ( optInclude.value and cmbRepository.listObject &lt;&gt; null );
end;
</source>
        <access>protected</access>
        <ReturnType>
            <type name="Boolean"/>
        </ReturnType>
    </JadeMethod>
    <JadeMethod name="load" id="080fda30-6faa-4b4c-9dd8-bf8317720354">
        <controlMethod name="Form::load"/>
        <methodInvocation>event</methodInvocation>
        <updating>true</updating>
        <source>load() updating;

begin
	inheritMethod();

	// Update captions
	optInclude.caption := "Add " &amp; schemaName &amp; " to repository";
	optExclude.caption := "Exclude " &amp; schemaName;
	
	// Resize form width so repository combo box remains anchored at same point (irrespective of schema name length)
	width := width - (optInclude.parentRightOffset - optInclude.getPersistentObject.OptionButton.parentRightOffset);
end;
</source>
    </JadeMethod>
    <JadeMethod name="optExclude_change" id="4ea75fb0-73e6-43e5-b233-9bc050be908f">
        <controlMethod name="OptionButton::change"/>
        <methodInvocation>event</methodInvocation>
        <updating>true</updating>
        <source>optExclude_change(optionbutton: OptionButton input) updating;

begin
	enableControls;
end;
</source>
        <Parameter name="optionbutton">
            <usage>input</usage>
            <type name="OptionButton"/>
        </Parameter>
    </JadeMethod>
    <JadeMethod name="optInclude_change" id="d84f9b6d-94ce-48a2-b364-fb2094434cff">
        <controlMethod name="OptionButton::change"/>
        <methodInvocation>event</methodInvocation>
        <updating>true</updating>
        <source>optInclude_change(optionbutton: OptionButton input) updating;

begin
	enableControls;
end;
</source>
        <Parameter name="optionbutton">
            <usage>input</usage>
            <type name="OptionButton"/>
        </Parameter>
    </JadeMethod>
    <JadeMethod name="setSchemaName" id="65043f0a-0eb9-4386-9e63-76838ed74991">
        <updating>true</updating>
        <source>setSchemaName(value: String): SelfType updating;

begin
	schemaName := value;	
	return self;
end;
</source>
        <Parameter name="value">
            <type name="String"/>
        </Parameter>
        <ReturnType>
            <type name="SelfType"/>
        </ReturnType>
    </JadeMethod>
</GUIClass>

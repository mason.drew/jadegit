#pragma once
#include "FileSystem.h"

namespace JadeGit
{
	class NativeFile;
	class NativeFileIterator;

	class NativeFileSystem : public FileSystem
	{
	public:
		NativeFileSystem(const std::filesystem::path& base, bool writable = false, const std::string& author = "", std::time_t modified = 0);
		NativeFileSystem(const std::filesystem::path& base, bool writable, const std::string& author, const std::string& modified);
		~NativeFileSystem();

		bool isReadOnly() const override;
		File open(const std::filesystem::path& path) const override;

	protected:
		friend NativeFile;
		friend NativeFileIterator;
		const std::filesystem::path base;
		const bool writable = false;
		const std::time_t modified;
		const std::string author;
	};
}
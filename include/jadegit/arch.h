#pragma once

#ifdef JADEGIT_STATIC
#  define JADEGIT_EXPORT
#else
#  ifndef JADEGIT_EXPORT
#    ifdef jadegit_EXPORTS
#      define JADEGIT_EXPORT __declspec(dllexport)
#    else
#      define JADEGIT_EXPORT __declspec(dllimport)
#    endif
#  endif
#endif

#ifndef JADEGIT_DEPRECATED
#  define JADEGIT_DEPRECATED __declspec(deprecated)
#endif

#ifndef JADEGIT_DEPRECATED_EXPORT
#  define JADEGIT_DEPRECATED_EXPORT JADEGIT_EXPORT JADEGIT_DEPRECATED
#endif

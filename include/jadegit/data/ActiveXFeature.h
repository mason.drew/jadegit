#pragma once
#include "Object.h"
#include "RootSchema/ActiveXFeatureMeta.h"

namespace JadeGit::Data
{
	class Feature;

	class ActiveXFeature : public Object
	{
	public:
		ActiveXFeature(Feature* parent, const Class* dataClass);

		ObjectValue<Feature* const, &ActiveXFeatureMeta::feature> feature;
	};

	extern template ObjectValue<Feature* const, &ActiveXFeatureMeta::feature>;
}
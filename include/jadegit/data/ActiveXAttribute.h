#pragma once
#include "ActiveXFeature.h"

namespace JadeGit::Data
{
	class Property;

	class ActiveXAttribute : public ActiveXFeature
	{
	public:
		ActiveXAttribute(Property* parent, const Class* dataClass);
	};
}
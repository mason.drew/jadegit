#pragma once
#include "ActiveXFeature.h"

namespace JadeGit::Data
{
	class Method;

	class ActiveXMethod : public ActiveXFeature
	{
	public:
		ActiveXMethod(Method* parent, const Class* dataClass);
	};
}
#pragma once
#include "SchemaEntity.h"
#include "EntityDict.h"
#include "Set.h"
#include "RootSchema/DatabaseMeta.h"
#include "RootSchema/DbClassMapMeta.h"
#include "RootSchema/DbFileMeta.h"

namespace JadeGit::Data
{
	class DbFile;
	class DbClassMap;
	class Schema;

	class Database : public SchemaEntity
	{
	public:
		Database(Schema* parent, const Class* dataClass, const char* name);
		
		ObjectValue<Schema* const, &DatabaseMeta::schema> schema;
		Value<DbFile*> defaultFile;
		ObjectValue<Set<DbClassMap*>, &DatabaseMeta::classMaps> classMaps;
		EntityDict<DbFile, &DatabaseMeta::dbFiles> files;

		void Accept(EntityVisitor &v) override;
	};

	extern template ObjectValue<Schema* const, &DatabaseMeta::schema>;
	extern template Value<DbFile*>;
	extern template ObjectValue<Set<DbClassMap*>, &DatabaseMeta::classMaps>;
	extern template EntityDict<DbFile, &DatabaseMeta::dbFiles>;

	class Class;

	class DbClassMap : public Object
	{
	public:
		DbClassMap(Class* parent, const Class* dataClass);

		enum class Mode : char
		{
			SharedInstances = 0,
			SubobjectInstances = 1,
			AllInstances = 2
		};

		ObjectValue<Database*, &DbClassMapMeta::database> database;
		ObjectValue<Class* const, &DbClassMapMeta::diskClass> diskClass;
		ObjectValue<DbFile*, &DbClassMapMeta::diskFile> diskFile;
		Value<Mode> mode = Mode::SharedInstances;

	protected:
		void LoadHeader(const FileElement& source) override;
		void WriteHeader(tinyxml2::XMLElement* element, const Object* origin, bool reference) const override;
	};

	extern template ObjectValue<Database*, &DbClassMapMeta::database>;
	extern template ObjectValue<Class* const, &DbClassMapMeta::diskClass>;
	extern template ObjectValue<DbFile*, &DbClassMapMeta::diskFile>;
	extern template Value<DbClassMap::Mode>;
	extern template std::map<DbClassMap::Mode, const char*> EnumStrings<DbClassMap::Mode>::data;

	class DbFile : public SchemaEntity
	{
	public:
		DbFile(Database* database, const Class* dataClass, const char* name);

		ObjectValue<Database* const, &DbFileMeta::database> database;
		ObjectValue<Set<DbClassMap*>, &DbFileMeta::classMapRefs> classMapRefs;

		void Accept(EntityVisitor &v) override;

		const Entity* GetQualifiedParent() const override;
	};

	extern template ObjectValue<Database* const, &DbFileMeta::database>;
	extern template ObjectValue<Set<DbClassMap*>, &DbFileMeta::classMapRefs>;
}
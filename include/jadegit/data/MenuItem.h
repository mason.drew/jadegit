#pragma once
#include "Object.h"
#include "ObjectValue.h"
#include "RootSchema/MenuItemMeta.h"

namespace JadeGit::Data
{
	class MenuItemData : public Object
	{
	public:
		using Object::Object;
	};

	class Form;

	class MenuItem : public MenuItemData
	{
	public:
		MenuItem(Form* parent, const Class* dataClass);

		enum class ShortCutFlags : int
		{
			None = 0,
			Shift = 4,
			Ctrl = 8,
			Shift_Ctrl = 12,
			Alt = 16,
			Shift_Alt = 20,
			Ctrl_Alt = 24,
			Shift_Ctrl_Alt = 28
		};

		enum class ShortCutKey : char
		{
			None = 0,
			Bksp = 8,
			Tab = 9,
			Return = 13,
			PageUp = 33,
			PageDown = 34,
			End = 35,
			Home = 36,
			LeftArrow = 37,
			UpArrow = 38,
			RightArrow = 39,
			DownArrow = 40,
			Ins = 45,
			Del = 46,
			N0 = 48,
			N1,
			N2,
			N3,
			N4,
			N5,
			N6,
			N7,
			N8,
			N9,
			A = 65,
			B,
			C,
			D,
			E,
			F,
			G,
			H,
			I,
			J,
			K,
			L,
			M,
			N,
			O,
			P,
			Q,
			R,
			S,
			T,
			U,
			V,
			W,
			X,
			Y,
			Z,
			F1 = 112,
			F2,
			F3,
			F4,
			F5,
			F6,
			F7,
			F8,
			F9,
			F10,
			F11,
			F12
		};

		ObjectValue<Form* const, &MenuItemMeta::form> form;
		Value<std::string> name;
		Value<ShortCutFlags> shortCutFlags = ShortCutFlags::None;
		Value<ShortCutKey> shortCutKey = ShortCutKey::None;
	};

	extern template Value<MenuItem::ShortCutFlags>;
	extern template Value<MenuItem::ShortCutKey>;
	extern template std::map<MenuItem::ShortCutFlags, const char*> EnumStrings<MenuItem::ShortCutFlags>::data;
	extern template std::map<MenuItem::ShortCutKey, const char*> EnumStrings<MenuItem::ShortCutKey>::data;
}
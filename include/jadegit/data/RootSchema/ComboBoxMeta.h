#pragma once
#include "ControlMeta.h"

namespace JadeGit::Data
{
	class ComboBoxMeta : public RootClass<GUIClass>
	{
	public:
		static const ComboBoxMeta& get(const Object& object);
		
		ComboBoxMeta(RootSchema& parent, const ControlMeta& superclass);
	
		PrimAttribute* const defaultLineHeight;
		PrimAttribute* const hasPictures;
		PrimAttribute* const hasPlusMinus;
		PrimAttribute* const hasTreeLines;
		PrimAttribute* const listWidth;
		PrimAttribute* const maxLength;
		PrimAttribute* const pictureClosed;
		PrimAttribute* const pictureLeaf;
		PrimAttribute* const pictureMinus;
		PrimAttribute* const pictureOpen;
		PrimAttribute* const picturePlus;
		PrimAttribute* const sortCased;
		PrimAttribute* const sorted;
		PrimAttribute* const style;
	};
};

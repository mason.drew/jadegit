#pragma once
#include "FeatureMeta.h"

namespace JadeGit::Data
{
	class JadeImportedFeatureMeta : public RootClass<>
	{
	public:
		static const JadeImportedFeatureMeta& get(const Object& object);
		
		JadeImportedFeatureMeta(RootSchema& parent, const FeatureMeta& superclass);
	
		ExplicitInverseRef* const importedClass;
		ExplicitInverseRef* const importedInterface;
		PrimAttribute* const incomplete;
	};
};

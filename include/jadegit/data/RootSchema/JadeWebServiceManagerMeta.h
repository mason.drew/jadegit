#pragma once
#include "ObjectMeta.h"

namespace JadeGit::Data
{
	class JadeWebServiceManagerMeta : public RootClass<>
	{
	public:
		static const JadeWebServiceManagerMeta& get(const Object& object);
		
		JadeWebServiceManagerMeta(RootSchema& parent, const ObjectMeta& superclass);
	
		PrimAttribute* const allowedSchemes;
		ExplicitInverseRef* const application;
		PrimAttribute* const provider;
		PrimAttribute* const supportLibrary;
	};
};

#pragma once
#include "TypeUsageMeta.h"

namespace JadeGit::Data
{
	class JadeLocalVarMeta : public RootClass<>
	{
	public:
		static const JadeLocalVarMeta& get(const Object& object);
		
		JadeLocalVarMeta(RootSchema& parent, const TypeUsageMeta& superclass);
	};
};

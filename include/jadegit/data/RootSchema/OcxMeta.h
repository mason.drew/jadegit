#pragma once
#include "ControlMeta.h"

namespace JadeGit::Data
{
	class OcxMeta : public RootClass<GUIClass>
	{
	public:
		static const OcxMeta& get(const Object& object);
		
		OcxMeta(RootSchema& parent, const ControlMeta& superclass);
	
		PrimAttribute* const autoSize;
	};
};

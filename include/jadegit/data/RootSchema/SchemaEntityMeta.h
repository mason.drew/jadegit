#pragma once
#include "ObjectMeta.h"

namespace JadeGit::Data
{
	class SchemaEntityMeta : public RootClass<>
	{
	public:
		static const SchemaEntityMeta& get(const Object& object);
		
		SchemaEntityMeta(RootSchema& parent, const ObjectMeta& superclass);
	
		PrimAttribute* const wsdlName;
		PrimAttribute* const abstract;
		PrimAttribute* const access;
		PrimAttribute* const helpKeyword;
		PrimAttribute* const name;
		PrimAttribute* const subAccess;
		PrimAttribute* const text;
		PrimAttribute* const userText;
	};
};

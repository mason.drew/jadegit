#pragma once
#include "TypeUsageMeta.h"

namespace JadeGit::Data
{
	class ReturnTypeMeta : public RootClass<>
	{
	public:
		static const ReturnTypeMeta& get(const Object& object);
		
		ReturnTypeMeta(RootSchema& parent, const TypeUsageMeta& superclass);
	
		PrimAttribute* const length;
	};
};

#pragma once
#include "ReferenceMeta.h"

namespace JadeGit::Data
{
	class ImplicitInverseRefMeta : public RootClass<>
	{
	public:
		static const ImplicitInverseRefMeta& get(const Object& object);
		
		ImplicitInverseRefMeta(RootSchema& parent, const ReferenceMeta& superclass);
	
		PrimAttribute* const memberTypeInverse;
	};
};

#pragma once
#include "ControlMeta.h"

namespace JadeGit::Data
{
	class ButtonMeta : public RootClass<GUIClass>
	{
	public:
		static const ButtonMeta& get(const Object& object);
		
		ButtonMeta(RootSchema& parent, const ControlMeta& superclass);
	
		PrimAttribute* const autoSize;
		PrimAttribute* const buttonPicture;
		PrimAttribute* const cancel;
		PrimAttribute* const caption;
		PrimAttribute* const default_;
		PrimAttribute* const picture;
		PrimAttribute* const pictureDisabled;
		PrimAttribute* const pictureDown;
		PrimAttribute* const style;
		PrimAttribute* const value;
		PrimAttribute* const webFileName;
	};
};

#pragma once
#include "ObjectMeta.h"

namespace JadeGit::Data
{
	class MenuItemDataMeta : public RootClass<GUIClass>
	{
	public:
		static const MenuItemDataMeta& get(const Object& object);
		
		MenuItemDataMeta(RootSchema& parent, const ObjectMeta& superclass);
	};
};

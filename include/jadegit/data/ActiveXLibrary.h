#pragma once
#include "Entity.h"
#include "Array.h"
#include <jadegit/data/RootSchema/ActiveXLibraryMeta.h>

namespace JadeGit::Data
{
	class ActiveXClass;
	class Schema;

	DECLARE_OBJECT_CAST(ActiveXClass)

	class ActiveXLibrary : public Entity
	{
	public:
		ActiveXLibrary(Schema* parent, const Class* dataClass, const char* name);

		ObjectValue<Array<ActiveXClass*>, &ActiveXLibraryMeta::coClasses> classes;
		Value<Binary> guid;
		ObjectValue<Schema* const, &ActiveXLibraryMeta::_schema> schema;

		void Accept(EntityVisitor& v) override;

		Class* getBaseClass() const;
	};

	extern template ObjectValue<Array<ActiveXClass*>, &ActiveXLibraryMeta::coClasses>;
	extern template ObjectValue<Schema* const, &ActiveXLibraryMeta::_schema>;
}
#pragma once
#include <string>

namespace JadeGit
{
	class Version
	{
	public:
		int major, minor, revision, build;

		constexpr Version(int major = 0, int minor = 0, int revision = 0, int build = 0) : major(major), minor(minor), revision(revision), build(build)
		{
		}

		Version(const char* version);
		Version(const std::string& version);

		operator std::string() const;

		bool empty() const;

		auto operator<=>(const Version&) const = default;
	};
}
#include <Approvals.h>
#include <catch2/catch_test_macros.hpp>
#include <jadegit/data/Assembly.h>
#include <jadegit/data/Class.h>
#include <jadegit/data/RootSchema.h>
#include <jadegit/data/RootSchema/ObjectMeta.h>
#include <jadegit/data/RootSchema/ApplicationMeta.h>
#include <jadegit/data/RootSchema/FormMeta.h>
#include <jadegit/vfs/MemoryFileSystem.h>
#include <SourceFileSystem.h>

using namespace std;
using namespace JadeGit;
using namespace JadeGit::Data;

TEST_CASE("Class.Creation", "[data]") 
{
	MemoryFileSystem fs;
	Assembly assembly(fs);

	Schema schema(assembly, nullptr, "TestSchema");
	schema.superschema = assembly.GetRootSchema();

	{
		Class klass(&schema, nullptr, "TestClass");

		// Check name has been set
		CHECK("TestClass" == klass.GetName());
		CHECK("TestClass" == klass.GetValue("name").Get<string>());

		// Check class has been added to collections
		CHECK(&klass == schema.classes.Get("TestClass"));
		CHECK(&klass == schema.getType("TestClass"));
	}

	// Check class has been removed from collections
	CHECK(nullptr == schema.classes.Get("TestClass"));
	CHECK(nullptr == schema.getType("TestClass"));
}

TEST_CASE("Class.Rename", "[data]") 
{
	MemoryFileSystem fs(SourceFileSystem("resources/Class/Basic"));
	Assembly assembly(fs);

	// Load existing schema
	auto& schema = Entity::resolve<Schema&>(assembly, "TestSchema");

	// Load existing class
	Class* klass = Entity::resolve<Class>(schema, "TestClass", false);
	REQUIRE(klass);

	// Verify class is in collections with current name
	CHECK(klass == schema.classes.Get("TestClass"));
	CHECK(klass == schema.getType("TestClass"));

	// Rename class
	klass->Rename("TestClassRenamed");

	// Check name change has been applied
	CHECK("TestClassRenamed" == klass->name);
	CHECK(nullptr == schema.classes.Get("TestClass"));
	CHECK(klass == schema.classes.Get("TestClassRenamed"));
	CHECK(nullptr == schema.getType("TestClass"));
	CHECK(klass == schema.getType("TestClassRenamed"));

	// Save changes
	assembly.save();

	// Check output matches expected
	ApprovalTests::Approvals::verify(fs);
}

TEST_CASE("Class.SuperclassInverseMaintenance", "[data]") 
{
	MemoryFileSystem fs;
	Assembly assembly(fs);

	Schema schema(assembly, nullptr, "TestSchema");
	schema.superschema = assembly.GetRootSchema();

	Class superclass(&schema, nullptr, "TestSuperclass");

	{
		Class klass(&schema, nullptr, "TestClass");

		// Set superclass
		klass.superclass = &superclass;

		// Check class has been added to superclass subclasses collection
		CHECK(superclass.subclasses.Includes(&klass));
	}

	// Check subclasses has been cleared
	CHECK(superclass.subclasses.Empty());
}

TEST_CASE("Class.WriteObjectSuperclass", "[data]") 
{
	MemoryFileSystem fs(SourceFileSystem("resources/Schema/Basic"));
	Assembly assembly(fs);

	// Load existing schema
	Schema* schema = Entity::resolve<Schema>(assembly, "TestSchema", true);
	REQUIRE(schema);

	// Create class
	Class klass(schema, nullptr, "TestClass");
	klass.Created("47183823-2574-4bfd-b411-99ed177d3e43");

	// Set superclass to root schema object class
	klass.superclass = static_cast<Class*>(*(assembly.GetRootSchema().object));

	// Save changes
	assembly.save();

	// Check output matches expected
	ApprovalTests::Approvals::verify(fs);
}

TEST_CASE("Class.WriteLocalSubclass", "[data]") 
{
	MemoryFileSystem fs(SourceFileSystem("resources/Class/Basic"));
	Assembly assembly(fs);

	// Load existing schema
	auto& schema = Entity::resolve<Schema&>(assembly, "TestSchema", true);

	// Load super class
	Class* superclass = Entity::resolve<Class>(schema, "TestClass", true);
	REQUIRE(superclass);

	Class subclass(&schema, nullptr, "TestSubclass");
	subclass.Created("47183823-2574-4bfd-b411-99ed177d3e43");
	
	subclass.superclass = superclass;

	// Save changes
	assembly.save();

	// Check output matches expected
	ApprovalTests::Approvals::verify(fs);
}

TEST_CASE("Class.InheritsFrom", "[data]") 
{
	MemoryFileSystem fs;
	Assembly assembly(fs);

	Schema schema(assembly, nullptr, "TestSchema");
	schema.superschema = assembly.GetRootSchema();

	// Make some local subschema copies
	Class object(&schema, nullptr, "Object");
	CHECK(object.superschemaType);

	Class application(&schema, nullptr, "Application");
	CHECK(application.superschemaType);

	GUIClass form(&schema, nullptr, "Form");
	CHECK(form.superschemaType);

	// Make some subclasses
	Class testApp(&schema, nullptr, "TestApplication", &application);
	Class testForm(&schema, nullptr, "TestForm", &form);

	// Check InheritsForm returns expected results
	CHECK(testApp.InheritsFrom(&testApp));
	CHECK(testApp.InheritsFrom(&object));
	CHECK(testApp.InheritsFrom(&application));

	CHECK(testForm.InheritsFrom(&object));
	CHECK(testForm.InheritsFrom(&form));

	CHECK(!application.InheritsFrom(&testApp));
	CHECK(!testApp.InheritsFrom(&form));
	CHECK(!testForm.InheritsFrom(&application));

	// Check InheritsForm returns expected results for superschema types
	const RootSchema& rootSchema = schema.GetRootSchema();
	CHECK(testApp.InheritsFrom(*rootSchema.object));
	CHECK(testApp.InheritsFrom(*rootSchema.application));

	CHECK(testForm.InheritsFrom(*rootSchema.object));
	CHECK(testForm.InheritsFrom(*rootSchema.form));

	CHECK(!testApp.InheritsFrom(*rootSchema.form));
	CHECK(!testForm.InheritsFrom(*rootSchema.application));
}
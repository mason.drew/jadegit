#include <catch2/catch_test_macros.hpp>
#include <jadegit/data/Assembly.h>
#include <jadegit/data/RootSchema.h>
#include <jadegit/vfs/MemoryFileSystem.h>

using namespace std;
using namespace JadeGit;
using namespace JadeGit::Data;

TEST_CASE("Database.Creation", "[data]") 
{
	MemoryFileSystem fs;
	Assembly assembly(fs);

	Schema schema(assembly, nullptr, "TestSchema");

	{
		Database database(&schema, nullptr, "TestDatabase");

		// Check name has been set as expected
		CHECK("TestDatabase" == database.GetName());
		CHECK("TestDatabase" == database.GetValue("name").Get<string>());

		// Check database has been added to collection
		CHECK(&database == schema.databases.Get("TestDatabase"));
	}

	// Check implicit deletion has removed database from collection
	CHECK(nullptr == schema.databases.Get("TestDatabase"));
}

TEST_CASE("Database.ClassMapsInverseMaintenance", "[data]") 
{
	MemoryFileSystem fs;
	Assembly assembly(fs);

	Schema schema(assembly, nullptr, "TestSchema");
	schema.superschema = assembly.GetRootSchema();

	Class klass(&schema, nullptr, "TestClass");
	DbClassMap classMap(&klass, nullptr);

	{
		Database database(&schema, nullptr, "TestDatabase");

		// Adding class map to database should set inverse
		database.classMaps.Add(&classMap);
		CHECK(&database == classMap.database);
	}

	// Check class map database has been reset implicitly when database is deleted
	CHECK(nullptr == static_cast<Database*>(classMap.database));
}
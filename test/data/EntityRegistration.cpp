#include <catch2/catch_test_macros.hpp>
#include <data/EntityRegistration.h>

using namespace JadeGit::Data;

TEST_CASE("EntityRegistration.Alias", "[data]") 
{
	CHECK(defaultEntityAlias("Class") == "class");
	CHECK(defaultEntityAlias("ExternalMethod") == "external method");
	CHECK(defaultEntityAlias("HTMLClass") == "HTML class");
	CHECK(defaultEntityAlias("JadeMethod") == "jade method");
}
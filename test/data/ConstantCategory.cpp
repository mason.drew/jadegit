#include <catch2/catch_test_macros.hpp>
#include <jadegit/data/Assembly.h>
#include <jadegit/vfs/MemoryFileSystem.h>

using namespace std;
using namespace JadeGit;
using namespace JadeGit::Data;

TEST_CASE("ConstantCategory.Creation", "[data]") 
{
	MemoryFileSystem fs;
	Assembly assembly(fs);

	Schema schema(assembly, nullptr, "TestSchema");

	{
		ConstantCategory category(&schema, nullptr, "TestCategory");

		// Check name has been set as expected
		CHECK("TestCategory" == category.GetName());
		CHECK("TestCategory" == category.GetValue("name").Get<string>());

		// Check category has been added to collection
		CHECK(&category == schema.constantCategories.Get("TestCategory"));
	}

	// Check implicit deletion has removed category from collection
	CHECK(nullptr == schema.constantCategories.Get("TestCategory"));
}

TEST_CASE("ConstantCategory.ConstantsInverseMaintenance", "[data]") 
{
	MemoryFileSystem fs;
	Assembly assembly(fs);

	Schema schema(assembly, nullptr, "TestSchema");

	GlobalConstant constant(&schema, nullptr, "TestConstant");

	{
		ConstantCategory category(&schema, nullptr, "TestCategory");

		// Adding constant to category should set inverse
		category.constants.Add(&constant);
		CHECK(&category == constant.category);
	}

	// Check constant category has been reset implicitly when category is deleted
	CHECK(nullptr == static_cast<ConstantCategory*>(constant.category));
}
#include <Approvals.h>
#include <catch2/catch_test_macros.hpp>
#include <jadegit/vfs/MemoryFileSystem.h>
#include <deploy/PowerShellDeploymentBuilder.h>

using namespace JadeGit;
using namespace JadeGit::Build;
using namespace JadeGit::Deploy;

TEST_CASE("PowerShellDeploymentBuilder.Empty", "[deploy]")
{
	MemoryFileSystem fs;

	// Setup builder
	PowerShellDeploymentBuilder builder(fs);

	// Empty deployment
	builder.start("", "", "");
	builder.finish("", "");

	// Check output matches expected
	ApprovalTests::Approvals::verify(fs);
}

TEST_CASE("PowerShellDeploymentBuilder.SchemaFiles", "[deploy]")
{
	MemoryFileSystem fs;

	// Setup builder
	PowerShellDeploymentBuilder builder(fs);

	// Start deployment & stage
	builder.start("fake-origin", "fake-commit-before", "fake-commit-after");

	// Add schema files
	auto schemaFile = builder.AddSchemaFile("TestSchema", false);
	*schemaFile << "schema definition" << std::flush;

	auto schemaDataFile = builder.AddSchemaDataFile("TestSchema", false);
	*schemaDataFile << "schema data" << std::flush;

	// Finish stage & deployment
	builder.finish("fake-origin", "fake-commit-after");

	// Check output matches expected
	ApprovalTests::Approvals::verify(fs);
}

TEST_CASE("PowerShellDeploymentBuilder.SchemaFilesWithInferredReorg", "[deploy]")
{
	MemoryFileSystem fs;

	// Setup builder
	PowerShellDeploymentBuilder builder(fs);

	// Start deployment & stage
	builder.start("fake-origin", "fake-commit-before", "fake-commit-after");

	// Add schema files
	auto schemaFile = builder.AddSchemaFile("TestSchema", true);
	*schemaFile << "schema definition" << std::flush;

	auto schemaDataFile = builder.AddSchemaDataFile("TestSchema", true);
	*schemaDataFile << "schema data" << std::flush;

	// Reorg should be inferred at end of stage

	// Finish stage & deployment
	builder.finish("fake-origin", "fake-commit-after");

	// Check output matches expected
	ApprovalTests::Approvals::verify(fs);
}

TEST_CASE("PowerShellDeploymentBuilder.CommandFile", "[deploy]")
{
	MemoryFileSystem fs;

	// Setup builder
	PowerShellDeploymentBuilder builder(fs);

	// Start deployment & stage
	builder.start("fake-origin", "fake-commit-before", "fake-commit-after");

	// Add command file
	auto commandFile = builder.AddCommandFile(false);
	*commandFile << "commands" << std::flush;

	// Finish stage & deployment
	builder.finish("fake-origin", "fake-commit-after");

	// Check output matches expected
	ApprovalTests::Approvals::verify(fs);
}

TEST_CASE("PowerShellDeploymentBuilder.CommandFileWithInferredReorg", "[deploy]")
{
	MemoryFileSystem fs;

	// Setup builder
	PowerShellDeploymentBuilder builder(fs);

	// Start deployment & stage
	builder.start("fake-origin", "fake-commit-before", "fake-commit-after");

	// Add command file
	auto commandFile = builder.AddCommandFile(true);
	*commandFile << "commands" << std::flush;

	// Reorg should be inferred at end of stage

	// Finish stage & deployment
	builder.finish("fake-origin", "fake-commit-after");

	// Check output matches expected
	ApprovalTests::Approvals::verify(fs);
}

TEST_CASE("PowerShellDeploymentBuilder.MultipleFilesWithExplicitReorgs", "[deploy]")
{
	MemoryFileSystem fs;

	// Setup builder
	PowerShellDeploymentBuilder builder(fs);

	// Start deployment & stage
	builder.start("fake-origin", "fake-commit-before", "fake-commit-after");

	// Add initial commands
	auto commandFile = builder.AddCommandFile(true);
	*commandFile << "commands" << std::flush;

	// Add schema files
	auto schemaFile = builder.AddSchemaFile("TestSchema", true);
	*schemaFile << "schema definition" << std::flush;

	auto schemaDataFile = builder.AddSchemaDataFile("TestSchema", true);
	*schemaDataFile << "schema data" << std::flush;

	// Initial reorg
	builder.Reorg();

	// More commands
	commandFile = builder.AddCommandFile(true);
	*commandFile << "more commands" << std::flush;

	schemaFile = builder.AddSchemaFile("AnotherTestSchema", true);
	*schemaFile << "another schema definition" << std::flush;

	schemaDataFile = builder.AddSchemaDataFile("AnotherTestSchema", true);
	*schemaDataFile << "more schema data" << std::flush;

	// Final reorg
	builder.Reorg();

	// Finish stage & deployment
	builder.finish("fake-origin", "fake-commit-after");

	// Check output matches expected
	ApprovalTests::Approvals::verify(fs);
}

TEST_CASE("PowerShellDeploymentBuilder.MultipleFilesWithInferredReorgs", "[deploy]")
{
	MemoryFileSystem fs;

	// Setup builder
	PowerShellDeploymentBuilder builder(fs);

	// Start deployment & stage
	builder.start("fake-origin", "fake-commit-before", "fake-commit-after");

	// Add initial commands
	auto commandFile = builder.AddCommandFile(true);
	*commandFile << "commands" << std::flush;

	// Add schema files
	auto schemaFile = builder.AddSchemaFile("TestSchema", true);
	*schemaFile << "schema definition" << std::flush;

	auto schemaDataFile = builder.AddSchemaDataFile("TestSchema", true);
	*schemaDataFile << "schema data" << std::flush;

	// Initial re-org should be inferred by current version load below

	// More commands
	commandFile = builder.AddCommandFile(false);
	*commandFile << "more commands" << std::flush;

	schemaFile = builder.AddSchemaFile("AnotherTestSchema", true);
	*schemaFile << "another schema definition" << std::flush;

	schemaDataFile = builder.AddSchemaDataFile("AnotherTestSchema", true);
	*schemaDataFile << "more schema data" << std::flush;

	// Final reorg should be inferred by prior latest schema version loads

	// Finish stage & deployment
	builder.finish("fake-origin", "fake-commit-after");

	// Check output matches expected
	ApprovalTests::Approvals::verify(fs);
}

TEST_CASE("PowerShellDeploymentBuilder.DuplicateReorgs", "[deploy]")
{
	MemoryFileSystem fs;

	// Setup builder
	PowerShellDeploymentBuilder builder(fs);

	// Start deployment & stage
	builder.start("fake-origin", "fake-commit-before", "fake-commit-after");

	// Invoke reorg multiple times (which should be deduplicated)
	builder.Reorg();
	builder.Reorg();
	builder.Reorg();

	// Finish stage & deployment
	builder.finish("fake-origin", "fake-commit-after");

	// Check output matches expected
	ApprovalTests::Approvals::verify(fs);
}

TEST_CASE("PowerShellDeploymentBuilder.Scripts", "[deploy]")
{
	MemoryFileSystem fs;

	// Setup builder
	PowerShellDeploymentBuilder builder(fs);

	// Start deployment & stage
	builder.start("fake-origin", "fake-commit-before", "fake-commit-after");

	// Add scripts
	{
		Script script;
		script.schema = "TestSchema";
		script.app = "TestApp";
		script.executeClass = "JadeScript";
		script.executeMethod = "doSomething";
		builder.addScript(script);
	}
	{
		Script script;
		script.schema = "TestSchema";
		script.app = "TestApp";
		script.executeClass = "TestClass";
		script.executeMethod = "doSomethingWithParam";
		script.executeParam = "param-value";
		builder.addScript(script);
	}
	{
		Script script;
		script.schema = "TestSubSchema";
		script.app = "TestSubApp";
		script.executeClass = "TestClass";
		script.executeMethod = "doSomethingTransiently";
		script.executeTransient = true;
		builder.addScript(script);
	}
	{
		Script script;
		script.schema = "TestSchema";
		script.app = "TestApp";
		script.executeClass = "TestClass";
		script.executeMethod = "doSomethingTransientlyWithParam";
		script.executeParam = "param-value";
		script.executeTransient = true;
		builder.addScript(script);
	}
	{
		Script script;
		script.schema = "TestSchema";
		script.app = "TestApp";
		script.executeClass = "TestClass";
		script.executeMethod = "doSomethingStatically";
		script.executeTypeMethod = true;
		builder.addScript(script);
	}
	{
		Script script;
		script.schema = "TestSchema";
		script.app = "TestApp";
		script.executeClass = "TestClass";
		script.executeMethod = "doSomethingStaticallyWithParam";
		script.executeParam = "param-value";
		script.executeTypeMethod = true;
		builder.addScript(script);
	}
	{
		Script script;
		script.schema = "TestSchema";
		script.app = "TestApp";
		script.executeSchema = "AnotherTestSchema";
		script.executeClass = "AnotherTestClass";
		script.executeMethod = "doAnotherThing";
		builder.addScript(script);
	}
	{
		Script script;
		script.schema = "TestSchema";
		script.app = "TestApp";
		script.executeSchema = "AnotherTestSchema";
		script.executeClass = "AnotherTestClass";
		script.executeMethod = "doAnotherThingTransientlyWithParam";
		script.executeParam = "another-param-value";
		script.executeTransient = true;
		builder.addScript(script);
	}
	{
		Script script;
		script.schema = "TestSchema";
		script.app = "TestApp";
		script.executeSchema = "AnotherTestSchema";
		script.executeClass = "AnotherTestClass";
		script.executeMethod = "doAnotherThingStatically";
		script.executeTypeMethod = true;
		builder.addScript(script);
	}

	// Finish stage & deployment
	builder.finish("fake-origin", "fake-commit-after");

	// Check output matches expected
	ApprovalTests::Approvals::verify(fs);
}

TEST_CASE("PowerShellDeploymentBuilder.ScriptAfterCommandFileWithInferredReorg", "[deploy]")
{
	MemoryFileSystem fs;

	// Setup builder
	PowerShellDeploymentBuilder builder(fs);

	// Start deployment & stage
	builder.start("fake-origin", "fake-commit-before", "fake-commit-after");

	// Add command file
	auto commandFile = builder.AddCommandFile(true);
	*commandFile << "commands" << std::flush;

	// Re-org should be inferred here

	// Add script
	Script script;
	script.schema = "TestSchema";
	script.app = "TestApp";
	script.executeClass = "JadeScript";
	script.executeMethod = "doSomething";
	builder.addScript(script);

	// Finish stage & deployment
	builder.finish("fake-origin", "fake-commit-after");

	// Check output matches expected
	ApprovalTests::Approvals::verify(fs);
}

TEST_CASE("PowerShellDeploymentBuilder.ScriptAfterSchemaFileWithInferredReorg", "[deploy]")
{
	MemoryFileSystem fs;

	// Setup builder
	PowerShellDeploymentBuilder builder(fs);

	// Start deployment & stage
	builder.start("fake-origin", "fake-commit-before", "fake-commit-after");

	// Add schema file
	auto schemaFile = builder.AddSchemaFile("TestSchema", true);
	*schemaFile << "schema definition" << std::flush;

	// Re-org should be inferred here

	// Add script
	Script script;
	script.schema = "TestSchema";
	script.app = "TestApp";
	script.executeClass = "JadeScript";
	script.executeMethod = "doSomething";
	builder.addScript(script);

	// Finish stage & deployment
	builder.finish("fake-origin", "fake-commit-after");

	// Check output matches expected
	ApprovalTests::Approvals::verify(fs);
}

TEST_CASE("PowerShellDeploymentBuilder.ScriptAfterSchemaDataFileWithInferredReorg", "[deploy]")
{
	MemoryFileSystem fs;

	// Setup builder
	PowerShellDeploymentBuilder builder(fs);

	// Start deployment & stage
	builder.start("fake-origin", "fake-commit-before", "fake-commit-after");

	// Add schema data file
	auto schemaDataFile = builder.AddSchemaDataFile("TestSchema", true);
	*schemaDataFile << "schema data" << std::flush;

	// Re-org should be inferred here

	// Add script
	Script script;
	script.schema = "TestSchema";
	script.app = "TestApp";
	script.executeClass = "JadeScript";
	script.executeMethod = "doSomething";
	builder.addScript(script);

	// Finish stage & deployment
	builder.finish("fake-origin", "fake-commit-after");

	// Check output matches expected
	ApprovalTests::Approvals::verify(fs);
}
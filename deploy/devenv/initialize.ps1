$db = "https://secure.jadeworld.com/JADETech/latest/" + 
	  ${env:JADE_VERSION} + "-" + ${env:JADE_CONFIG} + "/JADE-" +
	  ${env:JADE_VERSION} + "-" + @{'A' = 'Ansi'; 'U' = 'Unicode'}[$env:JADE_CONFIG] + "-db.zip"

Write-Output("Initializing new database [" + $db + "]") 

(New-Object System.Net.WebClient).DownloadFile($db, "c:/db.zip")
Expand-Archive -Path c:/db.zip -DestinationPath $env:JADE_SYSTEM
rm c:/db.zip

jadregb path=$env:JADE_SYSTEM ini=$env:JADE_INI name= 'JADE Container World' key=C7B23D8ACEBEB2B0A6B2B0B39085B2B2	

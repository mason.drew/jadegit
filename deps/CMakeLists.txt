# Build static libs if not building shared
if(NOT BUILD_SHARED_LIBS)
	set(BUILD_STATIC_LIBS ON CACHE BOOL "Build static libs")
endif()

# Add CLI11 (command line interface library)
add_subdirectory(CLI11)

# Add JADE if required
if(USE_JADE)
	if(PROJECT_IS_TOP_LEVEL AND BUILD_TESTING)
		set(JADE_FULL_DOWNLOAD ON CACHE BOOL "Enable full JADE download/setup" FORCE)
	endif()
	add_subdirectory(jade)
endif()

# Add libgit2 if required
if(USE_GIT2)
	set(BUILD_TESTS OFF CACHE BOOL "Build libgit2 tests" FORCE)
	set(STATIC_CRT OFF CACHE BOOL "Link the static CRT libraries" FORCE)
	set(USE_BUNDLED_ZLIB ON CACHE BOOL "Use the bundled version of zlib" FORCE)
	add_subdirectory(libgit2)

	# TODO: Raise issue with libgit2?
	target_include_directories(libgit2package PUBLIC
		${libgit2_SOURCE_DIR}/include
		${libgit2_BINARY_DIR}/include
		${libgit2_BINARY_DIR}/include/git2
	)

	# Suppress use of deprecated functions and values
	target_compile_definitions(libgit2package PUBLIC -DGIT_DEPRECATE_HARD)

	# Disable strict http parser mode to allow hostnames with an underscore
	target_compile_definitions(http-parser PUBLIC -DHTTP_PARSER_STRICT=0)

	# Use precompiled headers
	target_precompile_headers(libgit2
		PRIVATE
		<git2.h>
		${libgit2_SOURCE_DIR}/src/util/git2_util.h
	)
	target_precompile_headers(libgit2package
		PRIVATE
		<git2.h>
		${libgit2_SOURCE_DIR}/src/util/git2_util.h
	)
	target_precompile_headers(util
		PRIVATE
		${libgit2_SOURCE_DIR}/src/util/git2_util.h
	)
endif()

# Add proposed std uuid library
set(UUID_BUILD_TESTS OFF CACHE BOOL "Build the unit tests" FORCE)
set(UUID_USING_CXX20_SPAN ON CACHE BOOL "Using span from std instead of gsl" FORCE)
add_subdirectory(stduuid)

# Add tinyxml2
set(tinyxml2_BUILD_TESTING OFF)
add_subdirectory(tinyxml2)

# Add toml++
add_subdirectory(tomlplusplus)

# Add test frameworks
if(BUILD_TESTING)
	add_subdirectory(approvaltests)
	target_precompile_headers(ApprovalTests PRIVATE
		<iostream>
		<stdexcept>
		<string>
	)

	add_subdirectory(catch2)
	include(catch2/extras/Catch.cmake)
	target_precompile_headers(Catch2 PRIVATE
		<chrono>
		<iostream>
		<stdexcept>
		<string>
	)
endif()
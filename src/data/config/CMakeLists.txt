target_link_libraries(jadegit PUBLIC tomlplusplus::tomlplusplus)
target_compile_definitions(jadegit PUBLIC TOML_HEADER_ONLY=0)

target_sources(jadegit PRIVATE
	${CMAKE_CURRENT_LIST_DIR}/Config.cpp
	${CMAKE_CURRENT_LIST_DIR}/DeployConfig.cpp
)
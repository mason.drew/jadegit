#include "State.h"
#include <jadegit/data/Assembly.h>
#include <jadegit/data/Class.h>
#include <jadegit/data/Entity.h>
#include <jadegit/data/Reference.h>
#include <Epilog.h>
#include <ranges>
#include "storage/ObjectFileStorage.h"

namespace JadeGit::Data
{
	const State* State::get(const Object* object) const
	{
		return object->state;
	}

	void State::clean(Object* object) const
	{
		// Copy collection to clean, as deletion from original affects iteration
		std::pmr::vector<Object*> children(object->children);

		// Clean each child
		for (Object* child : children)
			child->Clean();
	}

	// When state transition confirms existence, object is moved to end of parent collection (maintaining correct order for output)
	void State::confirm(Object* object) const
	{
		auto parent = object->getParentObject();
		if (!parent)
			return;

		auto& v = parent->children;
		if (v.back() == object)
			return;

		auto pos = std::find(v.begin(), v.end(), object);
		std::rotate(pos, std::next(pos), v.end());
	}

	void State::created(Entity* entity, const char* guid) const
	{
		throw jadegit_exception("Invalid state for creation");
	}

	void State::dirty(Object* object, bool deep) const
	{
		throw jadegit_exception("Invalid state for change");
	}

	void State::load(Entity* entity) const
	{
		throw jadegit_exception("Invalid state for load");
	}

	void State::loading(Object* object, bool shallow) const
	{
		throw jadegit_exception("Invalid state for load to start");
	}

	void State::loaded(Object* object, bool shallow, bool resolved) const
	{
		throw jadegit_exception("Invalid state for load to finish");
	}

	void State::modified(Object* object) const
	{
		throw jadegit_exception("Invalid state for change");
	}

	void State::save(Entity* entity, bool prelude) const
	{
		throw jadegit_exception("Invalid state for save");
	}

	void State::saveChildren(const Object* parent, bool prelude) const
	{
		for (Object* child : parent->children)
		{
			Entity* entity = dynamic_cast<Entity*>(child);
			if (entity && entity->isMajor())
				entity->Save(prelude);
		}
	}

	void State::set(Object* object, const State* state) const
	{
		object->state = state;
		state->set(object);
	}

	// Object is up-to-date
	class CurrentState : public State, public Singleton<CurrentState>
	{
	public:
		// Existing object can be marked as dirty
		void dirty(Object* object, bool deep) const final;

		// Existing object can be updated
		void modified(Object* object) const final;

		// Load has already been completed
		void load(Entity* entity) const final {}

		// No local changes to save, but do need to recursively check children
		void save(Entity* entity, bool prelude) const final
		{
			saveChildren(entity, prelude);
		}

		// Load cannot be repeated
		void loading(Object* object, bool shallow) const final
		{
			throw jadegit_exception("Object cannot be reloaded");
		}

	protected:
		// When set, need to propagate down
		using State::set;
		void set(Object* parent) const final
		{
			// Replicate state change down to all children, which aren't inferred, nor saved in their own right (minor entities)
			for (Object* child : parent->children)
				if (!child->isInferred() && !child->isMajor())
					set(child, this);
		}
	};

	// Intermediary loaded state used after initial load, until post-load tasks in the queue are completed
	template <bool Shallow>
	class LoadedState : public State, public Singleton<LoadedState<Shallow>>
	{
	public:
		// Suppress calls to mark object as modified while loading
		void modified(Object* object) const override {}

		void load(Entity* entity) const override
		{
			if constexpr (Shallow)
			{
				// Load can only be restarted for entities for which file path can be derived
				assert(entity->isMajor());

				// Reload definition via assembly, which should return current entity
				if (entity != entity->getAssembly().Load(entity->path(), false))
					throw jadegit_exception("Reload failed (" + entity->GetQualifiedName() + ")");

				// Verify state change occurred
				assert(get(entity) == LoadedState<false>::Instance());
			}
		}

		// Load has finished
		void loaded(Object* object, bool shallow, bool resolved) const final;

		// Load can be restarted
		void loading(Object* object, bool shallow) const final;
	};

	bool State::isCurrent() const
	{
		return this == CurrentState::Instance() || this == LoadedState<false>::Instance();
	}

	// Intermediary loading state used during initial load
	class LoadingState : public State, public Singleton<LoadingState>
	{
	public:
		// Suppress calls to mark object as modified while loading
		void modified(Object* object) const override {}

		// Load hasn't actually been completed yet, but we're currently ignoring subsequent calls which reflects previous behaviour
		// TODO: Further refactoring needed to distinguish when to ignore during final load phase when references are being resolved, but parent/child structure has been loaded
		void load(Entity* entity) const override
		{
//			throw jadegit_exception("Invalid state for load");
		}

		// Initial load has finished
		void loaded(Object* object, bool shallow, bool resolved) const override
		{
			assert(!resolved);
			if (shallow)
				set(object, LoadedState<true>::Instance());
			else
				set(object, LoadedState<false>::Instance());
		}
	};

	bool State::isLoading() const
	{
		return this == LoadingState::Instance();
	}

	// Load can be restarted
	void LoadedState<true>::loading(Object* object, bool shallow) const
	{
		// Must be completing load in this scenario
		if (shallow)
			throw jadegit_exception("Invalid state for load");

		set(object, LoadingState::Instance());
	}

	void LoadedState<false>::loading(Object* object, bool shallow) const
	{
		State::loading(object, shallow);
	}

	// Known entity has been instantiated, but not fully loaded
	class ShallowState : public State, public Singleton<ShallowState>
	{
	public:
		// Load can be completed
		void load(Entity* entity) const final
		{
			// Load can only be restarted for entities for which file path can be derived
			assert(entity->isMajor());

			// Reload definition via assembly, which should return current entity
			if (entity != entity->getAssembly().Load(entity->path(), false))
				throw jadegit_exception("Reload failed (" + entity->GetQualifiedName() + ")");

			// Verify state change occurred
			assert(get(entity)->isCurrent());
		}

		// Load can be restarted
		void loading(Object* object, bool shallow) const final
		{
			// Must be completing load in this scenario
			if (shallow)
				throw jadegit_exception("Invalid state for load");

			set(object, LoadingState::Instance());
		}

		// No local changes to save, but do need to recursively check children
		void save(Entity* entity, bool prelude) const final
		{
			saveChildren(entity, prelude);
		}
	};

	bool State::isShallow() const
	{
		return this == ShallowState::Instance() || this == LoadedState<true>::Instance();
	}

	void LoadedState<true>::loaded(Object* object, bool shallow, bool resolved) const
	{
		assert(resolved);
		assert(shallow);
		set(object, ShallowState::Instance());
	}

	void LoadedState<false>::loaded(Object* object, bool shallow, bool resolved) const
	{
		assert(resolved);
		if (!shallow) set(object, CurrentState::Instance());
	}

	// Object has been modified in some way which is yet to be saved
	class ModifiedState : public State
	{
	public:
		// Modified object children may be marked as dirty
		void dirty(Object* object, bool deep) const final
		{
			// In this scenario, the parent may have already been implicitly created/updated due to child updates

			// Flag children as being dirty (assuming parent state change was implicit, some may still be redundant)
			for (int i = 0; i < object->children.size(); i++)
			{
				Object* child = object->children[i];
				if (deep || !dynamic_cast<Entity*>(child))
					child->Dirty(deep);
			}
		}

		// Change has already been registered
		void modified(Object* object) const override {}

		// Load has already been completed
		void load(Entity* entity) const override {}

		void save(Entity* entity, bool prelude) const override = 0;

	protected:
		// Modified entity can be saved
		void save(Entity* entity, bool prelude, bool adding) const
		{
			// Recurse up to parent entity with which definition file is associated
			if (!entity->isMajor())
				return entity->getParentEntity()->Save(prelude);

			// Suppress write during initial prelude to validate whether all entities can be saved
			if (!prelude)
			{
				// Save to file repository
				entity->getAssembly().getStorage().write(*entity, adding);

				// Set state to current
				set(entity, CurrentState::Instance());
			}

			// Save changes to children stored separately
			saveChildren(entity, prelude);
		}

		// When set, need to propagate state up
		using State::set;
		void set(Object* child) const final
		{
			// No need to propagate any further if we've reached a major entity that'll be saved in it's own right
			if (child->isMajor())
				return;

			// Set appropriate modified state via parent method
			child->getParentObject()->Modified();
		}
	};

	// New object is being created
	class CreatedState : public ModifiedState, public Singleton<CreatedState>
	{
	public:
		using ModifiedState::save;
		void save(Entity* entity, bool prelude) const override
		{
			save(entity, prelude, true);
		}
	};

	// Existing object is being updated
	class UpdatedState : public ModifiedState, public Singleton<UpdatedState>
	{
	public:
		using ModifiedState::save;
		void save(Entity* entity, bool prelude) const override
		{
			save(entity, prelude, false);
		}
	};

	bool State::isModified() const
	{
		return this == CreatedState::Instance() || this == UpdatedState::Instance();
	}

	// Existing object can be updated
	void CurrentState::modified(Object* object) const
	{
		set(object, UpdatedState::Instance());
	}

	// Existing object is dirty, where it may be discarded if not modified before clean-up
	class DirtyState : public State, public Singleton<DirtyState>
	{
	public:
		// Dirty object can be cleaned, in which case it's deleted
		void clean(Object* object) const final
		{
			Object::dele(object);
		}

		// Object already marked as dirty
		void dirty(Object* object, bool deep) const final {}

		// Dirty object can be updated, making it valid again
		void modified(Object* object) const final
		{
			// Confirm existence again if modified order needs to preserved/reflected by extract
			auto parent = object->getParentObject();
			if (parent && parent->isOrganized())
				confirm(object);

			set(object, UpdatedState::Instance());
		}

		// Load completed before being marked as dirty
		void load(Entity* entity) const final {}
	};

	// Existing object can be marked as dirty
	void CurrentState::dirty(Object* object, bool deep) const
	{
		set(object, DirtyState::Instance());

		// Flag children as being dirty
		for (int i = 0; i < object->children.size(); i++)
		{
			Object* child = object->children[i];
			if (deep || !dynamic_cast<Entity*>(child))
				child->Dirty(deep);
		}
	}

	// Implied object may be loaded, confirming it's a known entity
	void ImpliedState::loading(Object* object, bool shallow) const
	{
		// Confirm existence
		confirm(object);

		set(object, LoadingState::Instance());
	}

	// Unless loaded, entity is unknown and cannot be saved
	void ImpliedState::save(Entity* entity, bool prelude) const
	{
		throw jadegit_exception("Unknown " + entity->dataClass->name + " (" + entity->GetQualifiedName() + ")");
	}

	// Inferred object may be loaded, implying that it's a known entity
	void InferredState::loading(Object* object, bool shallow) const
	{
		// Confirm existence
		confirm(object);

		set(object, LoadingState::Instance());
	}

	// Inferred object can be modified, implying it should be created/saved
	void InferredState::modified(Object* object) const
	{
		// Confirm existence
		confirm(object);

		// Reset back to initial state and repeat
		set(object, InitialState::Instance());
		object->Modified();
	}

	template<class T = std::mt19937, std::size_t N = T::state_size * sizeof(typename T::result_type)>
	T makeRandomEngine()
	{
		std::random_device source;
		auto random_data = std::views::iota(std::size_t(), (N - 1) / sizeof(source()) + 1) | std::views::transform([&](auto) { return source(); });
		std::seed_seq seeds(std::begin(random_data), std::end(random_data));
		return T(seeds);
	}

	// New entity may be explicitly created with guid supplied
	void InitialState::created(Entity* entity, const char* id) const
	{
		thread_local auto engine = makeRandomEngine();
		thread_local uuids::uuid_random_generator generate(engine);

		assert(entity->id.is_nil());
		entity->id = id ? uuids::uuid::from_string(id).value() : generate();
		set(entity, CreatedState::Instance());
	}

	// New entity can be implied
	void InitialState::implied(Entity* entity) const
	{
		set(entity, ImpliedState::Instance());
	}

	// New entity can be inferred
	void InitialState::inferred(Entity* entity) const
	{
		set(entity, InferredState::Instance());
	}

	// Existing object can be loaded
	void InitialState::loading(Object* object, bool shallow) const
	{
		set(object, LoadingState::Instance());
	}

	// New object can be modified, implying creation
	void InitialState::modified(Object* object) const
	{
		// Confirm existence
		confirm(object);

		if (Entity* entity = dynamic_cast<Entity*>(object))
			created(entity, nullptr);
		else
			set(object, CreatedState::Instance());
	}

	// Unless loaded or modified, entity is unknown and cannot be saved
	void InitialState::save(Entity* entity, bool prelude) const
	{
		throw jadegit_exception("Unknown " + entity->dataClass->name + " (" + entity->GetQualifiedName() + ")");
	}
}
#include <jadegit/data/ActiveXLibrary.h>
#include <jadegit/data/ActiveXClass.h>
#include <jadegit/data/ActiveXAttribute.h>
#include <jadegit/data/ActiveXConstant.h>
#include <jadegit/data/ActiveXMethod.h>
#include <jadegit/data/CollClass.h>
#include <jadegit/data/Schema.h>
#include <jadegit/data/EntityVisitor.h>
#include <jadegit/data/RootSchema.h>
#include <jadegit/data/RootSchema/ActiveXClassMeta.h>
#include <jadegit/data/RootSchema/ActiveXAttributeMeta.h>
#include <jadegit/data/RootSchema/ActiveXConstantMeta.h>
#include <jadegit/data/RootSchema/ActiveXMethodMeta.h>
#include <jadegit/data/RootSchema/ActiveXInterfaceMeta.h>
#include <jadegit/data/RootSchema/ActiveXLibraryMeta.h>
#include "EntityRegistration.h"

namespace JadeGit::Data
{
	DEFINE_OBJECT_CAST(ActiveXClass)
	DEFINE_OBJECT_CAST(ActiveXFeature)

	static EntityRegistration<ActiveXLibrary, Schema> activeXLibrary("ActiveXLibrary", "external component", &Schema::activeXLibraries);
	static ObjectRegistration<ActiveXClass, Class> activeXClass("ActiveXClass");
	static ObjectRegistration<ActiveXAttribute, Property> activeXAttribute("ActiveXAttribute");
	static ObjectRegistration<ActiveXConstant, Constant> activeXConstant("ActiveXConstant");
	static ObjectRegistration<ActiveXMethod, Method> activeXMethod("ActiveXMethod");

	template ObjectValue<Array<ActiveXClass*>, &ActiveXLibraryMeta::coClasses>;
	template ObjectValue<Schema* const, &ActiveXLibraryMeta::_schema>;

	ActiveXLibrary::ActiveXLibrary(Schema* parent, const Class* dataClass, const char* name) : Entity(parent, dataClass ? dataClass : GetDataClass(parent, &RootSchema::activeXLibrary), name),
		schema(parent)
	{
	}

	void ActiveXLibrary::Accept(EntityVisitor& v)
	{
		v.Visit(this);
	}

	Class* ActiveXLibrary::getBaseClass() const
	{
		for (auto& activeXClass : classes)
		{
			if (activeXClass->baseClass->name == name)
				return activeXClass->baseClass;
		}

		return nullptr;
	}

	template ObjectValue<ActiveXLibrary*, &ActiveXClassMeta::activeXLibrary>;
	template ObjectValue<Class* const, &ActiveXClassMeta::baseClass>;

	ActiveXClass::ActiveXClass(Class* parent, const Class* dataClass) : Object(parent, dataClass ? dataClass : GetDataClass(parent, &RootSchema::activeXClass)),
		baseClass(parent)
	{
	}

	template ObjectValue<Feature* const, &ActiveXFeatureMeta::feature>;

	ActiveXFeature::ActiveXFeature(Feature* parent, const Class* dataClass) : Object(parent, dataClass),
		feature(parent)
	{
	}

	ActiveXAttribute::ActiveXAttribute(Property* parent, const Class* dataClass) : ActiveXFeature(parent, dataClass ? dataClass : GetDataClass(parent, &RootSchema::activeXAttribute))
	{
	}

	ActiveXConstant::ActiveXConstant(Constant* parent, const Class* dataClass) : ActiveXFeature(parent, dataClass ? dataClass : GetDataClass(parent, &RootSchema::activeXConstant))
	{
	}

	ActiveXMethod::ActiveXMethod(Method* parent, const Class* dataClass) : ActiveXFeature(parent, dataClass ? dataClass : GetDataClass(parent, &RootSchema::activeXMethod))
	{
	}

	ActiveXClassMeta::ActiveXClassMeta(RootSchema& parent, const ObjectMeta& superclass) : RootClass(parent, "ActiveXClass", superclass),
		activeXLibrary(NewReference<ExplicitInverseRef>("activeXLibrary", NewType<Class>("ActiveXLibrary"))),
		activeXName(NewString("activeXName", 255)),
		baseClass(NewReference<ExplicitInverseRef>("baseClass", NewType<Class>("Class"))),
		defaultEventInterfaceGuid(NewBinary("defaultEventInterfaceGuid", 16)),
		defaultInterfaceGuid(NewBinary("defaultInterfaceGuid", 16)),
		dotNetName(NewStringUtf8("dotNetName")),
		executeMode(NewByte("executeMode")),
		guid(NewBinary("guid", 16)),
		isEventInterface(NewBoolean("isEventInterface")),
		key(NewBinary("key"))
	{
		activeXLibrary->bind(&ActiveXClass::activeXLibrary);
		baseClass->manual().parent().bind(&ActiveXClass::baseClass);
	}

	ActiveXFeatureMeta::ActiveXFeatureMeta(RootSchema& parent, const ObjectMeta& superclass) : RootClass(parent, "ActiveXFeature", superclass),
		feature(NewReference<ExplicitInverseRef>("feature", NewType<Class>("Feature"))),
		memid(NewInteger("memid"))
	{
		feature->manual().parent().bind(&ActiveXFeature::feature);
	}

	ActiveXAttributeMeta::ActiveXAttributeMeta(RootSchema& parent, const ActiveXFeatureMeta& superclass) : RootClass(parent, "ActiveXAttribute", superclass) {}

	ActiveXConstantMeta::ActiveXConstantMeta(RootSchema& parent, const ActiveXFeatureMeta& superclass) : RootClass(parent, "ActiveXConstant", superclass),
		activeXName(NewString("activeXName", 256)) {}

	ActiveXMethodMeta::ActiveXMethodMeta(RootSchema& parent, const ActiveXFeatureMeta& superclass) : RootClass(parent, "ActiveXMethod", superclass) {}

	ActiveXInterfaceMeta::ActiveXInterfaceMeta(RootSchema& parent, const ObjectMeta& superclass) : RootClass(parent, "ActiveXInterface", superclass) {}

	ActiveXLibraryMeta::ActiveXLibraryMeta(RootSchema& parent, const ObjectMeta& superclass) : RootClass(parent, "ActiveXLibrary", superclass),
		_schema(NewReference<ExplicitInverseRef>("_schema", NewType<Class>("Schema"))),
		coClasses(NewReference<ExplicitInverseRef>("coClasses", NewType<CollClass>("ActiveXClassGuidDict"))),
		componentType(NewInteger("componentType")),
		guid(NewBinary("guid", 16)),
		helpFileName(NewString("helpFileName", 257)),
		status(NewInteger("status")),
		uuid(NewBinary("uuid", 16))
	{
		coClasses->automatic().bind(&ActiveXLibrary::classes);
		guid->bind(&ActiveXLibrary::guid);
		_schema->manual().parent().bind(&ActiveXLibrary::schema);
	}
}
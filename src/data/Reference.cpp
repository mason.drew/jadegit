#include <jadegit/data/Reference.h>
#include <jadegit/data/EntityVisitor.h>
#include <jadegit/data/RootSchema.h>
#include <jadegit/data/RootSchema/ExternalReferenceMeta.h>
#include <jadegit/data/RootSchema/InverseMeta.h>
#include "FeatureRegistration.h"
#include "State.h"

namespace JadeGit::Data
{
	DEFINE_OBJECT_CAST(Reference)

	template <class TDerived>
	class ExplicitInverseRefRegistration : protected PropertyRegistration<TDerived>
	{
	public:
		using PropertyRegistration<TDerived>::PropertyRegistration;

	protected:
		using PropertyRegistration<TDerived>::Resolve;

		TDerived* Resolve(const Component* origin, const QualifiedName* name, bool expected, bool shallow, bool inherit) const override
		{
			// Use basic resolution when no name has been specified
			if (!name)
				return this->Resolve(origin, expected);

			// Determine if reference can be implied for inverse
			bool imply = !inherit && shallow && expected && dynamic_cast<const TDerived*>(origin) != nullptr;

			// Resolve parent
			Class* parent = EntityFactory::Get().Resolve<Class>(origin, name->parent.get(), expected, imply || is_major_entity<TDerived>, inherit);
			if (!parent)
				return nullptr;

			// Imply reference exists if parent hasn't already been loaded
			if (imply && (parent->isShallow() || parent->isLoading()))
			{
				// Find & return existing
				if (TDerived* result = this->lookup(parent, name->name))
					return result;

				// Instantiate implied reference
				TDerived* result = this->CreateInstance<Class, const Class*, const char*>(parent, nullptr, name->name.c_str());
				result->implied();
				return result;
			}

			// Attempt to resolve from collection
			if (TDerived* result = PropertyRegistration<TDerived>::Resolve(parent, name->name, shallow, inherit))
				return result;

			if (expected)
				throw jadegit_exception("Failed to resolve reference [" + std::string(*name) + "]");

			return nullptr;
		}
	};

	class InverseRegistration : protected ObjectRegistration<Inverse, ExplicitInverseRef>
	{
	public:
		InverseRegistration() : ObjectRegistration("Inverse") {};

	protected:
		using ObjectRegistration::Resolve;

		Inverse* load(Component* origin, const Class* dataClass, const FileElement& source) const override
		{
			// Get qualified reference name attribute
			auto path = source.header("name");
			if (!path) throw jadegit_exception("Missing inverse reference name attribute");

			// Convert to qualified name
			QualifiedName name(path);

			// Resolve parent (left)
			ExplicitInverseRef* left = EntityFactory::Get().Resolve<ExplicitInverseRef>(origin, nullptr);

			// Resolve inverse reference (right)
			ExplicitInverseRef* right = EntityFactory::Get().Resolve<ExplicitInverseRef>(left->dataClass->name, left, &name, true, true, false);

			// Return existing (possibly already instantiated by counterpart)
			if (Inverse* result = left->inverses[right])
				return result;

			// Create new
			return new Inverse(left, right);
		}
	};

	static InverseRegistration inverse;
	static ExplicitInverseRefRegistration<ExplicitInverseRef> explicitInverseRef("ExplicitInverseRef");
	static ExplicitInverseRefRegistration<JadeDynamicExplicitInverseRef> dynamicExplicitInverseRef("JadeDynamicExplicitInverseRef");
	static PropertyRegistration<ImplicitInverseRef> implicitInverseRef("ImplicitInverseRef");
	static PropertyRegistration<JadeDynamicImplicitInverseRef> dynamicImplicitInverseRef("JadeDynamicImplicitInverseRef");

	std::map<ExplicitInverseRef::ReferenceKind, const char*> EnumStrings<ExplicitInverseRef::ReferenceKind>::data =
	{
		{ ExplicitInverseRef::ReferenceKind::Peer, "peer" },
		{ ExplicitInverseRef::ReferenceKind::Child, "child" },
		{ ExplicitInverseRef::ReferenceKind::Parent, "parent" }
	};

	std::map<ExplicitInverseRef::UpdateMode, const char*> EnumStrings<ExplicitInverseRef::UpdateMode>::data =
	{
		{ ExplicitInverseRef::UpdateMode::Manual, "manual" },
		{ ExplicitInverseRef::UpdateMode::Manual_Automatic, "manual/automatic" },
		{ ExplicitInverseRef::UpdateMode::Automatic, "automatic" },
		{ ExplicitInverseRef::UpdateMode::Automatic_Deferred, "automatic-deferred" },
		{ ExplicitInverseRef::UpdateMode::Manual_Automatic_Deferred, "manual/automatic-deferred" }
	};

	Inverse::Inverse(ExplicitInverseRef* left, ExplicitInverseRef* right) : Inverse(left, left == right ? this : new Inverse(right, this, InferredState::Instance()))
	{
		first = true;
		left->inverses[right] = this;
		right->inverses[left] = counterpart;
	}

	Inverse::~Inverse()
	{
		if (counterpart)
			counterpart->counterpart = nullptr;
	}

	Inverse::Inverse(ExplicitInverseRef* parent, Inverse* counterpart, const State* state) : Object(parent, GetDataClass(parent, &RootSchema::inverse), state), reference(parent), counterpart(counterpart) {}

	const Inverse* Inverse::getPrimary() const
	{
		// Return this if inverse refers back to self
		if (this == counterpart)
			return this;

		// Return parent inverse
		switch (getReferenceKind())
		{
		case ExplicitInverseRef::ReferenceKind::Parent:
			return this;
		case ExplicitInverseRef::ReferenceKind::Child:
			return counterpart;

		// Default to first inverse created
		default:
			return first ? this : counterpart;
		}
	}

	ExplicitInverseRef::ReferenceKind Inverse::getReferenceKind() const
	{
		if (!reference->isImplied())
			return reference->kind;

		switch (counterpart->reference->kind)
		{
		case ExplicitInverseRef::ReferenceKind::Parent:
			return ExplicitInverseRef::ReferenceKind::Child;
		case ExplicitInverseRef::ReferenceKind::Child:
			return ExplicitInverseRef::ReferenceKind::Parent;
		default:
			return ExplicitInverseRef::ReferenceKind::Peer;
		}
	}

	ExplicitInverseRef::UpdateMode Inverse::getReferenceUpdateMode() const
	{
		if (!reference->isImplied())
			return reference->updateMode;

		switch (counterpart->reference->updateMode)
		{
		case ExplicitInverseRef::UpdateMode::Automatic:
			return ExplicitInverseRef::UpdateMode::Manual;
		case ExplicitInverseRef::UpdateMode::Manual:
			return ExplicitInverseRef::UpdateMode::Automatic;
		default:
			return ExplicitInverseRef::UpdateMode::Manual_Automatic;
		}
	}

	void Inverse::WriteHeader(tinyxml2::XMLElement* element, const Object* origin, bool reference) const
	{
		// Inverses cannot be referred to
		assert(!reference);

		// Set name to inverse reference name
		if (counterpart)
			element->SetAttribute("name", counterpart->reference->GetQualifiedName(this->reference).c_str());
	}

	Reference::Reference(Class* parent, const Class* dataClass, const char* name, Type* type) : Property(parent, dataClass, name, type)
	{
	}

	AnyValue* Reference::InstantiateValue(Object& object) const
	{
		return type->CreateValue(object, *this, !embedded);
	}

	template Value<ExplicitInverseRef::ReferenceKind>;
	template Value<ExplicitInverseRef::UpdateMode>;

	ExplicitInverseRef::ExplicitInverseRef(Class* parent, const Class* dataClass, const char* name, Type* type) : Reference(parent, dataClass ? dataClass : GetDataClass(parent, &RootSchema::explicitInverseRef, "ExplicitInverseRef"), name, type) {}

	void ExplicitInverseRef::Accept(EntityVisitor &v)
	{
		v.Visit(this);
	}

	bool ExplicitInverseRef::WriteFilter() const
	{
		// Suppress writing automatic references
		if (updateMode == Automatic)
			return false;
		
		// Suppress writing parent references (implied by context)
		if (kind == Parent && updateMode == Manual)
			return false;

		// Suppress writing children (handled generically)
		if (kind == Child)
			throw jadegit_unimplemented_feature();

		return Reference::WriteFilter();
	}

	JadeDynamicExplicitInverseRef::JadeDynamicExplicitInverseRef(Class* parent, const Class* dataClass, const char* name) : ExplicitInverseRef(parent, dataClass ? dataClass : GetDataClass(parent, &RootSchema::jadeDynamicExplicitInverseRef), name) {}

	ImplicitInverseRef::ImplicitInverseRef(Class* parent, const Class* dataClass, const char* name, Type* type) : Reference(parent, dataClass ? dataClass : GetDataClass(parent, &RootSchema::implicitInverseRef, "ImplicitInverseRef"), name, type) {}

	void ImplicitInverseRef::Accept(EntityVisitor &v)
	{
		v.Visit(this);
	}

	JadeDynamicImplicitInverseRef::JadeDynamicImplicitInverseRef(Class* parent, const Class* dataClass, const char* name) : ImplicitInverseRef(parent, dataClass ? dataClass : GetDataClass(parent, &RootSchema::jadeDynamicImplicitInverseRef), name) {}

	InverseMeta::InverseMeta(RootSchema& parent, const ObjectMeta& superclass) : RootClass(parent, "Inverse", superclass) {}

	ReferenceMeta::ReferenceMeta(RootSchema& parent, const PropertyMeta& superclass) : RootClass(parent, "Reference", superclass),
		constraint(NewReference<ExplicitInverseRef>("constraint", NewType<Class>("JadeMethod")))
	{
		constraint->bind(&Reference::constraint);
	}

	ExplicitInverseRefMeta::ExplicitInverseRefMeta(RootSchema& parent, const ReferenceMeta& superclass) : RootClass(parent, "ExplicitInverseRef", superclass),
		inverseNotRequired(NewBoolean("inverseNotRequired")),
		kind(NewCharacter("kind")),
		updateMode(NewCharacter("updateMode")),
		transientToPersistentAllowed(NewBoolean("transientToPersistentAllowed"))
	{
		inverseNotRequired->bind(&ExplicitInverseRef::inverseNotRequired);
		kind->bind(&ExplicitInverseRef::kind);
		updateMode->SetDefault(Value(ExplicitInverseRef::Manual_Automatic))->bind(&ExplicitInverseRef::updateMode);
		transientToPersistentAllowed->bind(&ExplicitInverseRef::transientToPersistentAllowed);
	}

	ExternalReferenceMeta::ExternalReferenceMeta(RootSchema& parent, const ExplicitInverseRefMeta& superclass) : RootClass(parent, "ExternalReference", superclass),
		joinPredicate(NewString("joinPredicate")),
		joinPredicateInfo(NewBinary("joinPredicateInfo")) {}

	JadeDynamicExplicitInverseRefMeta::JadeDynamicExplicitInverseRefMeta(RootSchema& parent, const ExplicitInverseRefMeta& superclass) : RootClass(parent, "JadeDynamicExplicitInverseRef", superclass),
		dynamicPropertyCluster(NewReference<ExplicitInverseRef>("dynamicPropertyCluster", NewType<Class>("JadeDynamicPropertyCluster")))
	{
		dynamicPropertyCluster->bind(&JadeDynamicExplicitInverseRef::dynamicPropertyCluster);
	}

	ImplicitInverseRefMeta::ImplicitInverseRefMeta(RootSchema& parent, const ReferenceMeta& superclass) : RootClass(parent, "ImplicitInverseRef", superclass),
		memberTypeInverse(NewBoolean("memberTypeInverse"))
	{
		memberTypeInverse->bind(&ImplicitInverseRef::memberTypeInverse);
	}

	JadeDynamicImplicitInverseRefMeta::JadeDynamicImplicitInverseRefMeta(RootSchema& parent, const ImplicitInverseRefMeta& superclass) : RootClass(parent, "JadeDynamicImplicitInverseRef", superclass),
		dynamicPropertyCluster(NewReference<ExplicitInverseRef>("dynamicPropertyCluster", NewType<Class>("JadeDynamicPropertyCluster")))
	{
		dynamicPropertyCluster->bind(&JadeDynamicImplicitInverseRef::dynamicPropertyCluster);
	}
}
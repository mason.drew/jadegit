#pragma once
#include "Task.h"

namespace JadeGit::Build
{
	class Builder;

	class ReorgTask : public Task
	{
	public:
		using Task::Task;

		bool Execute(Builder& builder) const;

	protected:
		bool Accept(TaskVisitor& v) const override final;
		void Print(std::ostream& output) const override;
	};
}
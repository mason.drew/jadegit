#pragma once
#include <Graph.h>
#include "Task.h"
#include "Delta.h"
#include <jadegit/data/Assembly.h>
#include <jadegit/data/Entity.h>
#include <jadegit/data/EntityVisitor.h>
#include <assert.h>

namespace JadeGit::Build
{
	class TaskGraph : public Graph<Task>, Data::EntityVisitor
	{
	public:
		TaskGraph() : Graph([](const Task* task) { return task->Priority(); }) {}
		~TaskGraph();

		void Add(Data::Entity* entity, bool latest);
		bool analyze(IProgress* progress);

		template<class TDelta>
		TDelta* Find(const Data::Object* object)
		{
			auto entity = dynamic_cast<const Data::Entity*>(object);
			if (!entity)
				return nullptr;

			IDelta* delta = deltasByEntity[entity];
			return dynamic_cast<TDelta*>(delta);
		}

		template<class TDelta>
		TDelta* Analyze(const Data::Object* object)
		{
			auto entity = dynamic_cast<const Data::Entity*>(object);
			if (!entity)
				return nullptr;

			IDelta* delta = deltasByEntity[entity];

			if (delta)
				delta->Analyze();

			return dynamic_cast<TDelta*>(delta);
		}

	private:
		friend Task;
		friend TaskWaypoint;

		std::vector<Task*> commands;
		std::vector<IDelta*> deltas;
		std::map<Data::EntityKey, IDelta*> deltasById;
		std::map<const Data::Entity*, IDelta*> deltasByEntity;
		std::map<std::pair<const Data::Entity*, TaskWaypoint::Type>, TaskWaypoint*> waypoints;

		bool latest = false;

		void DuplicateCheck(const Data::Entity* prior, const Data::Entity* current) const;

		template<class TDelta, class TEntity, typename... Args>
		void Deltify(TEntity* entity, Args... args)
		{
			assert(!entity->isStatic() && !entity->isShallow());

			auto key = entity->getKey();

			IDelta* d = deltasById[key];
			TDelta* delta = dynamic_cast<TDelta*>(d);
			assert(!d == !delta);
			if (!delta)
			{
				deltasById[key] = delta = new TDelta(*this, args...);
				deltas.push_back(delta);
			}

			if (latest)
			{
				DuplicateCheck(delta->latest, entity);
				delta->latest = entity;
			}
			else
			{
				DuplicateCheck(delta->previous, entity);
				delta->previous = entity;
			}

			deltasByEntity[entity] = delta;
		}

		// EntityVisitor (ordered by type alphabetically)
		void Visit(Data::ActiveXLibrary* library) override;
		void Visit(Data::Application* application) override;
		void Visit(Data::Class* cls) override;
		void Visit(Data::CollClass* cls) override;
		void Visit(Data::CompAttribute* attribute) override;
		void Visit(Data::Constant* constant) override;
		void Visit(Data::ConstantCategory* category) override;
		void Visit(Data::Control* control) override;
		void Visit(Data::CurrencyFormat* format) override;
		void Visit(Data::Database* database) override;
		void Visit(Data::DateFormat* format) override;
		void Visit(Data::DbFile* dbFile) override;
		void Visit(Data::ExplicitInverseRef* reference) override;
		void Visit(Data::ExternalMethod* method) override;
		void Visit(Data::Form* form) override;
		void Visit(Data::Function* function) override;
		void Visit(Data::GlobalConstant* constant) override;
		void Visit(Data::ImplicitInverseRef* reference) override;
		void Visit(Data::JadeExportedClass* cls) override;
		void Visit(Data::JadeExportedConstant* constant) override;
		void Visit(Data::JadeExportedInterface* interface) override;
		void Visit(Data::JadeExportedMethod* method) override;
		void Visit(Data::JadeExportedPackage* package) override;
		void Visit(Data::JadeExportedProperty* property) override;
		void Visit(Data::JadeExposedClass* cls) override;
		void Visit(Data::JadeExposedFeature* feature) override;
		void Visit(Data::JadeExposedList* list) override;
		void Visit(Data::JadeHTMLDocument* document) override;
		void Visit(Data::JadeImportedClass* cls) override;
		void Visit(Data::JadeImportedConstant* constant) override;
		void Visit(Data::JadeImportedInterface* interface) override;
		void Visit(Data::JadeImportedMethod* method) override;
		void Visit(Data::JadeImportedPackage* package) override;
		void Visit(Data::JadeImportedProperty* property) override;
		void Visit(Data::JadeInterface* interface) override;
		void Visit(Data::JadeInterfaceMethod* method) override;
		void Visit(Data::JadeMethod* method) override;
		void Visit(Data::JadeWebServicesClass* cls) override;
		void Visit(Data::JadeWebServicesMethod* method) override;
		void Visit(Data::JadeWebServiceConsumerClass* cls) override;
		void Visit(Data::JadeWebServiceProviderClass* cls) override;
		void Visit(Data::JadeWebServiceSoapHeaderClass* cls) override;
		void Visit(Data::Library* library) override;
		void Visit(Data::Locale* locale) override;
		void Visit(Data::NumberFormat* format) override;
		void Visit(Data::PrimAttribute* attribute) override;
		void Visit(Data::PrimType* primitive) override;
		void Visit(Data::PseudoType* pseudoType) override;
		void Visit(Data::RelationalAttribute* attribute) override;
		void Visit(Data::RelationalTable* table) override;
		void Visit(Data::RelationalView* view) override;
		void Visit(Data::Schema* schema) override;
		void Visit(Data::TimeFormat* format) override;
		void Visit(Data::TranslatableString* string) override;
	};
}

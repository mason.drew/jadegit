#pragma once
#include "TaskVisitor.h"

namespace JadeGit::Build
{
	class FileBuilder : public TaskVisitor
	{
	public:
		virtual void Flush(bool final) = 0;
	};
}
#pragma once
#include "SchemaComponentDelta.h"
#include "BuildTask.h"
#include "CommandStrategy.h"
#include <jadegit/data/Schema.h>

namespace JadeGit::Build
{
	class SchemaBuildTask : public BuildTask
	{
	public:
		SchemaBuildTask(TaskGraph& graph, Schema& schema) : BuildTask(graph), schema(schema) {}

	protected:
		Schema& schema;
	};

	class RegisterSchemaTask : public SchemaBuildTask
	{
	public:
		using SchemaBuildTask::SchemaBuildTask;

		bool Execute(Builder& builder) const override
		{
			builder.RegisterSchema(schema.name.c_str());
			return true;
		}

		void Print(std::ostream& output) const
		{
			output << "Register Schema " << schema.name;
		}
	};

	class DeregisterSchemaTask : public SchemaBuildTask
	{
	public:
		using SchemaBuildTask::SchemaBuildTask;

		bool Execute(Builder& builder) const override
		{
			builder.DeregisterSchema(schema.name.c_str());
			return true;
		}

		void Print(std::ostream& output) const
		{
			output << "Deregister Schema " << schema.name;
		}
	};

	class SchemaDeletionTask : public CommandDeleteTask
	{
	public:
		using CommandDeleteTask::CommandDeleteTask;

		void Execute(CommandStrategy& strategy) const override
		{
			CommandDeleteTask::Execute(strategy);

			// Delete schemas via separate command files as JADE commits their deletion immediately so can't be repeated during a retry
			strategy.Flush();
		}
	};

	class ModifySchemaTask : public CommandTask
	{
	public:
		std::string schemaName;
		std::string key;
		std::string value;

		ModifySchemaTask(TaskGraph& graph, std::string schemaName, std::string key, std::string value) : CommandTask(graph, LoadStyle::Current), 
			schemaName(std::move(schemaName)), 
			key(std::move(key)),
			value(std::move(value))
		{}

		void Execute(CommandStrategy& strategy) const override
		{
			strategy.ModifySchema(schemaName, key, value);
		}

		void Print(std::ostream& output) const override
		{
			output << "Modify Schema " << schemaName << " (" << key << " = " << value << ")";
		}
	};

	class SchemaDelta : public SchemaComponentDelta<Schema>
	{
	public:
		SchemaDelta(TaskGraph& graph) : SchemaComponentDelta(graph, "Schema") {}
		
		using SchemaComponentDelta<Schema>::previous;

	protected:
		bool AnalyzeEnter() override
		{
			/* Analyze previous superschema */
			const SchemaDelta* previousSuperschema = nullptr;
			if (previous)
				previousSuperschema = graph.Analyze<SchemaDelta>(previous->superschema);
			
			/* Basic analysis */
			if (!SchemaComponentDelta::AnalyzeEnter())
			{
				/* Deregister schema */
				new DeregisterSchemaTask(graph, *previous);

				/* Handle deletion before deleting previous superschema */
				if (previousSuperschema)
					graph.AddEdge(GetDeletion(), previousSuperschema->GetDeletion());

				return false;
			}

			/* Register schema on creation */
			if (!previous)
				new RegisterSchemaTask(graph, *latest);

			/* Analyze latest superschema */
			const SchemaDelta* latestSuperschema = nullptr;
			latestSuperschema = graph.Analyze<SchemaDelta>(latest->superschema);

			/* Definition dependent on superschema being created */
			if (latestSuperschema)
				graph.AddEdge(latestSuperschema->GetCreation(), GetDefinition());

			// TODO: Handle schema moves


			// Handle schema modifications
			if (previous)
			{
				// Handle switching default locale
				if (previous->primaryLocale && latest->primaryLocale && previous->primaryLocale->name != latest->primaryLocale->name)
				{
					auto modification = new ModifySchemaTask(graph, QualifiedName(), "DefaultLocale", latest->primaryLocale->name);
					graph.AddEdge(GetRename(), modification);

					// Create new primary locale before switch
					if (auto latestPrimaryLocale = graph.Analyze<IDelta>(latest->primaryLocale))
						graph.AddEdge(latestPrimaryLocale->GetCreation(), modification);
					
					// Delete previous primary locale after switch
					if (auto previousPrimaryLocale = graph.Analyze<IDelta>(previous->primaryLocale))
						graph.AddEdge(modification, previousPrimaryLocale->GetDeletion());
				}
			}

			return true;
		}

		Task* HandleDeletion() override
		{
			return new SchemaDeletionTask(graph, entityType, QualifiedName());
		}
	};
}
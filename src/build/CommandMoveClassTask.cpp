#include "CommandMoveClassTask.h"
#include "CommandStrategy.h"

namespace JadeGit::Build
{
	CommandMoveClassTask::CommandMoveClassTask(TaskGraph &graph, std::string qualifiedName, std::string newSuperClass) : CommandTask(graph),
		qualifiedName(std::move(qualifiedName)),
		newSuperClass(std::move(newSuperClass))
	{
	}

	void CommandMoveClassTask::Execute(CommandStrategy &strategy) const
	{
		strategy.MoveClass(qualifiedName, newSuperClass);
	}

	void CommandMoveClassTask::Print(std::ostream& output) const
	{
		output << "Move Class " << qualifiedName << " " << newSuperClass;
	}
}
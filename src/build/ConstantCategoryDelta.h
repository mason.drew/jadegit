#pragma once
#include "SchemaComponentDelta.h"
#include <jadegit/data/ConstantCategory.h>

namespace JadeGit::Build
{
	class ConstantCategoryDelta : public SchemaComponentDelta<ConstantCategory>
	{
	public:
		ConstantCategoryDelta(TaskGraph& graph) : SchemaComponentDelta(graph, "GlobalConstantCategory") {}

	protected:
		bool AnalyzeEnter() override
		{
			/* Basic analysis */
			if (!SchemaComponentDelta::AnalyzeEnter())
			{
				/* Handle deleting dependent constants before category is deleted */
				for (GlobalConstant* constant : previous->constants)
				{
					if (const ISchemaComponentDelta* constantDelta = graph.Analyze<ISchemaComponentDelta>(constant))
						graph.AddEdge(constantDelta->GetDeletion(), GetDeletion());
				}

				return false;
			}

			return true;
		}
	};
}
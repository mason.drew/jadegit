#pragma once
#include "Task.h"

namespace JadeGit::Build
{
	class EmptyTask : public Task
	{
	public:
		using Task::Task;

		bool Accept(TaskVisitor& v) const override { return true; };
		void Print(std::ostream& output) const override { output << "<empty>"; };
	};
}
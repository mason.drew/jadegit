#pragma once
#include "ClassDelta.h"
#include <jadegit/data/ActiveXLibrary.h>

namespace JadeGit::Build
{
	class ActiveXLibraryDelta : public SchemaComponentDelta<ActiveXLibrary>
	{
	public:
		ActiveXLibraryDelta(TaskGraph& graph) : SchemaComponentDelta(graph, "ActiveXLibrary") {}

	protected:
		bool AnalyzeEnter() final
		{
			// Analyze base class
			baseClass = graph.Analyze<IClassDelta>(latest ? latest->getBaseClass() : previous->getBaseClass());

			// Basic analysis
			if (!SchemaComponentDelta::AnalyzeEnter())
				return false;

			return true;
		}

		Task* HandleDeletion() final
		{
			// Deletion implied by base class deletion
			if (baseClass)
				return baseClass->GetDeletion();

			return SchemaComponentDelta::HandleDeletion();
		}

		Task* HandleRename() final
		{
			// Rename implied by base class rename
			if (baseClass)
				return baseClass->GetRename();

			return SchemaComponentDelta::HandleRename();
		}

	private:
		IClassDelta* baseClass = nullptr;
	};
}
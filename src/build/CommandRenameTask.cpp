#include "CommandRenameTask.h"
#include "CommandStrategy.h"

namespace JadeGit::Build
{
	CommandRenameTask::CommandRenameTask(TaskGraph& graph, const char* entityType, std::string qualifiedName, std::string newName) : CommandTask(graph),
		entityType(entityType),
		qualifiedName(std::move(qualifiedName)),
		newName(std::move(newName))
	{
	}

	void CommandRenameTask::Execute(CommandStrategy& strategy) const
	{
		strategy.Rename(entityType, qualifiedName, newName);
	}

	void CommandRenameTask::Print(std::ostream& output) const
	{
		output << "Rename " << entityType << " " << qualifiedName << " " << newName;
	}
}
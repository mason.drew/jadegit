#pragma once
#include <jadegit/data/Entity.h>
#include "EmptyTask.h"

namespace JadeGit::Build
{
	class TaskGraph;

	class TaskWaypoint : public EmptyTask
	{
	public:
		enum Type
		{
			Definition,
			MoveSubclass
		};

		static TaskWaypoint* make(TaskGraph& graph, const Data::Entity* entity, Type type);

	protected:
		using EmptyTask::EmptyTask;
	};
}
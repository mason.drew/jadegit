#pragma once

namespace JadeGit::Data
{
	class ActiveXLibrary;
	class Application;
	class Class;
	class JadeExposedClass;
	class JadeExposedFeature;
	class JadeExposedList;
	class JadeHTMLDocument;
	class JadeInterfaceMapping;
	class Form;
	class RelationalAttribute;
	class RelationalTable;
	class RelationalView;
	class Schema;
	class SchemaEntity;
}

namespace JadeGit::Build
{
	class ExtractStrategy
	{
	public:
		virtual bool Declare(const Data::Class* klass) = 0;

		virtual bool Define(const Data::ActiveXLibrary* library, bool complete) = 0;
		virtual bool Define(const Data::Application* application, bool complete) = 0;
		virtual bool Define(const Data::JadeExposedClass* cls, bool complete) = 0;
		virtual bool Define(const Data::JadeExposedFeature* feature, bool complete) = 0;
		virtual bool Define(const Data::JadeExposedList* list, bool complete) = 0;
		virtual bool Define(const Data::JadeHTMLDocument* document, bool complete, const char* oldName = nullptr) = 0;
		virtual bool Define(const Data::JadeInterfaceMapping* mapping, bool complete) = 0;
		virtual bool Define(const Data::Form* form, bool complete) = 0;
		virtual bool Define(const Data::RelationalAttribute* attribute, bool complete) = 0;
		virtual bool Define(const Data::RelationalTable* table, bool complete) = 0;
		virtual bool Define(const Data::RelationalView* view, bool complete) = 0;
		virtual bool Define(const Data::Schema* schema, bool complete) = 0;
		virtual bool Define(const Data::SchemaEntity* entity, bool complete) = 0;
	};
}
#pragma once
#include "FileTask.h"
#include "ExtractStrategy.h"

namespace JadeGit::Build
{
	class ExtractTask : public FileTask
	{
	public:
		using FileTask::FileTask;
		ExtractTask(TaskGraph& graph, bool complete) : ExtractTask(graph, LoadStyle::Latest, complete) {}

		bool Accept(TaskVisitor &v) const override final;

		virtual bool Execute(ExtractStrategy &strategy) const = 0;
	};
}
#pragma once
#include "TypeDelta.h"
#include <jadegit/data/JadeInterface.h>

namespace JadeGit::Build
{
	class IJadeInterfaceDelta : public ITypeDelta
	{
	public:
		using ITypeDelta::ITypeDelta;

		// Nothing specific as yet
	};

	template<class TComponent, class TInterface = IJadeInterfaceDelta>
	class JadeInterfaceDelta : public TypeDelta<TComponent, TInterface>
	{
	public:
		JadeInterfaceDelta(TaskGraph &graph) : TypeDelta<TComponent, TInterface>(graph, "Interface") {}

		using TypeDelta<TComponent, TInterface>::previous;
		using TypeDelta<TComponent, TInterface>::latest;

	protected:
		using TypeDelta<TComponent, TInterface>::graph;
		using TypeDelta<TComponent, TInterface>::GetDefinition;
		using TypeDelta<TComponent, TInterface>::GetDeletion;

		bool AnalyzeEnter() override
		{
			/* Basic analysis */
			if (!TypeDelta<TComponent, TInterface>::AnalyzeEnter())
			{
				/* Handle deletion before deleting previous super-interfaces */
				for(JadeInterface* interface : previous->superinterfaces)
				{
					const IJadeInterfaceDelta* previousSuperInterface = graph.Analyze<IJadeInterfaceDelta>(interface);
					if(previousSuperInterface)
						graph.AddEdge(GetDeletion(), previousSuperInterface->GetDeletion());
				}

				return false;
			}

			/* Latest definition is dependent on latest super-interfaces being declared/defined */
			for (JadeInterface* interface : latest->superinterfaces)
			{
				const IJadeInterfaceDelta* latestSuperInterface = graph.Analyze<IJadeInterfaceDelta>(interface);
				if (latestSuperInterface)
					graph.AddEdge(latestSuperInterface->GetDeclaration(), GetDefinition());
			}

			return true;
		}
	};
}
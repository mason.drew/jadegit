#pragma once
#include "Delta.h"
#include "CommandDeleteTask.h"
#include "CommandRenameTask.h"
#include <jadegit/data/SchemaEntity.h>

namespace JadeGit::Build
{
	class ISchemaComponentDelta : public IDelta
	{
	public:
		using IDelta::IDelta;

		virtual std::string QualifiedName() const = 0;
		virtual Task* GetRename() const = 0;
		virtual bool ParentRenamed(Task* command) = 0;	
	};

	template<class TComponent, class TInterface = ISchemaComponentDelta>
	class SchemaComponentDelta : public Delta<TComponent, TInterface>
	{
	public:
		SchemaComponentDelta(TaskGraph& graph, const char* entityType) : Delta<TComponent, TInterface>(graph), entityType(entityType) {}

		using Delta<TComponent, TInterface>::previous;
		using Delta<TComponent, TInterface>::latest;

	protected:
		using Delta<TComponent, TInterface>::analyzed;
		using Delta<TComponent, TInterface>::graph;

		const char* const entityType;

		Task* GetCreation() const override
		{
			/* Rename is like creation (new entity name needs to be established before use) */
			return rename ? rename : Delta<TComponent, TInterface>::GetCreation();
		}

		Task* GetRename() const final
		{
			if (rename)
				return rename;

			if (previous && !analyzed)
			{
				const Data::Object* component = previous->getParentObject();
				while (component)
				{
					if (const ISchemaComponentDelta* delta = graph.Find<ISchemaComponentDelta>(component))
						return delta->GetRename();

					component = component->getParentObject();
				}
			}

			return nullptr;
		}

		std::string QualifiedName(const Data::Entity* entity) const
		{
			const Data::Entity* parent = entity->GetQualifiedParent();
			if (!parent)
				return entity->GetName();
			
			const ISchemaComponentDelta* delta = graph.Find<ISchemaComponentDelta>(parent);
			return (delta ? delta->QualifiedName() : QualifiedName(parent)) + "::" + entity->GetName();
		}

		std::string QualifiedName() const final
		{
			return QualifiedName((previous && !rename) ? previous : latest);
		}

		bool AnalyzeEnter() override
		{
			/* Returns false when deleted */
			if (!Delta<TComponent, TInterface>::AnalyzeEnter())
				return false;
			
			/* Analyze rename */
			if (previous && previous->GetName() != latest->GetName())
				AnalyzeRename();

			/* Rename before definition */
			graph.AddEdge(GetRename(), this->GetDefinition());

			return true;
		}

		Task* HandleDeletion() override
		{
			if (Task* task = Delta<TComponent, TInterface>::HandleDeletion())
				return task;

			// Not applicable to copies
			if (previous->isCopy())
				return nullptr;

			// Setup deletion command
			Task* deletion = new CommandDeleteTask(graph, entityType, QualifiedName());

			// Parent rename before deletion
			graph.AddEdge(GetRename(), deletion);

			return deletion;
		}

		void AnalyzeRename()
		{
			assert(!rename);
			assert(previous && previous->GetName() != latest->GetName());

			// Resolve parent rename (if any)
			Task* rename = GetRename();

			// Setup rename task
			this->rename = HandleRename();

			if (this->rename)
			{
				// Parent rename before child rename
				graph.AddEdge(rename, this->rename);

				// Notify existing children of rename
				this->Cascade<ISchemaComponentDelta>(previous, std::bind(&ISchemaComponentDelta::ParentRenamed, std::placeholders::_1, this->rename));
			}
		}

		virtual Task* HandleRename()
		{
			return new CommandRenameTask(graph, entityType, QualifiedName(), latest->GetName());
		}

		bool ParentRenamed(Task* command) override
		{
			/* Parent renames must occur after child commands already setup */
			if (analyzed)
			{
				graph.AddEdge(this->GetDeletion(), command);
				graph.AddEdge(rename, command);
			}

			/* Notify children */
			return true;
		}

	private:
		Task* rename = nullptr;
	};
}
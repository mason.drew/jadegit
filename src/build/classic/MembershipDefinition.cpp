#include "MembershipDefinition.h"
#include "SchemaDefinition.h"

namespace JadeGit::Build::Classic
{
	static NodeRegistration<MembershipDefinition, Data::CollClass> registrar;

	MembershipDefinition::MembershipDefinition(SchemaDefinition* schema, const Data::CollClass* klass) : source(klass)
	{
		schema->membershipDefinitions.push_back(this);
	}

	void MembershipDefinition::WriteBody(std::ostream& output, const std::string& indent)
	{
		output << indent << source->GetName() << " of " << source->memberType->GetName();
		
		if (source->memberTypeSize)
			output << "[" << source->memberTypeSize << "]";

		if (source->memberTypePrecision)
		{
			output << "[" << source->memberTypePrecision;
			if (source->memberTypeScaleFactor)
				output << "," << source->memberTypeScaleFactor;
			output << "]";
		}
		
		output << ";\n";
	}
}
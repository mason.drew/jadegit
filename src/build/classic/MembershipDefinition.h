#pragma once
#include "DefinitionNode.h"
#include <jadegit/data/CollClass.h>

namespace JadeGit::Build::Classic
{
	class MembershipDefinition : public DefinitionNode
	{
	public:
		using base_node = MembershipDefinition;
		using parent_node = SchemaDefinition;

		MembershipDefinition(SchemaDefinition* schema, const Data::CollClass* klass);

		void WriteBody(std::ostream& output, const std::string& indent) override;

	private:
		const Data::CollClass* source;
	};
}
#pragma once
#include "..\ExtractStrategy.h"
#include "SchemaDefinition.h"

namespace JadeGit::Build
{
	class Builder;
}

namespace JadeGit::Build::Classic
{
	class SchemaFileWriter : public ExtractStrategy
	{
	public:
		SchemaFileWriter(Builder &builder, bool latest);
		void Write();

	protected:
		bool Declare(const Data::Class* klass) final;

		bool Define(const Data::ActiveXLibrary* library, bool complete) final;
		bool Define(const Data::Application* application, bool complete) final;
		bool Define(const Data::JadeExposedClass* cls, bool complete) final;
		bool Define(const Data::JadeExposedFeature* feature, bool complete) final;
		bool Define(const Data::JadeExposedList* list, bool complete) final;
		bool Define(const Data::JadeHTMLDocument* document, bool complete, const char* oldName) final;
		bool Define(const Data::JadeInterfaceMapping* mapping, bool complete) final;
		bool Define(const Data::Form* form, bool complete) final;
		bool Define(const Data::RelationalAttribute* attribute, bool complete) final;
		bool Define(const Data::RelationalTable* table, bool complete) final;
		bool Define(const Data::RelationalView* view, bool complete) final;
		bool Define(const Data::Schema* schema, bool complete) final;
		bool Define(const Data::SchemaEntity* entity, bool complete) final;

	private:
		Builder &builder;
		SchemaDefinition root;
		const bool latest;
	};
}
#include "MemberKeyDictionaryDefinition.h"
#include "SchemaDefinition.h"

using namespace JadeGit::Data;

namespace JadeGit::Build::Classic
{
	static NodeRegistration<MemberKeyDictionaryDefinition, CollClass> dict;
	static NodeRegistration<MemberKeyDefinition, MemberKey> key;

	MemberKeyDictionaryDefinition::MemberKeyDictionaryDefinition(SchemaDefinition* schema, const CollClass* klass) : DictionaryDefinition(schema, klass)
	{
		schema->memberKeyDefinitions.push_back(this);
	}

	void MemberKeyDefinition::WriteBody(std::ostream& output, const std::string& indent)
	{
		output << indent;
		
		for (Property* property : source->keyPath)
			output << property->GetName() << ".";

		output << source->property->GetName();

		KeyDefinition::WriteBody(output, indent);
	}
}
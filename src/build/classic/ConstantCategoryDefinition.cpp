#include "ConstantCategoryDefinition.h"
#include "SchemaDefinition.h"
#include <jadegit/data/Type.h>

namespace JadeGit::Build::Classic
{
	class ConstantCategoryDefinitionRegistration : public NodeRegistration<ConstantCategoryDefinition, Data::ConstantCategory>
	{
	public:
		using NodeRegistration::NodeRegistration;

	protected:
		ConstantCategoryDefinition* Resolve(SchemaDefinition* schema, SchemaDefinition* parent, const Data::ConstantCategory* component, bool complete) const override
		{
			// Reuse last category definition created if component matches
			if (!parent->constantDefinitions.empty())
			{
				if (auto definition = parent->constantDefinitions.back())
					if (static_cast<ConstantCategoryDefinition*>(definition)->source == component)
						return static_cast<ConstantCategoryDefinition*>(definition);
			}

			// Otherwise, instantiate as new, which may be a duplicate for constants dependent on those extracted in other categories since initial extract
			// NOTE: This relies on SchemaDefinition destructor cleaning up category definition nodes explicitly
			return new ConstantCategoryDefinition(parent, component);
		}
	};
	static ConstantCategoryDefinitionRegistration registrar;

	ConstantCategoryDefinition::ConstantCategoryDefinition(SchemaDefinition* schema, const Data::ConstantCategory* category) : SchemaEntityDefinition(category), constantDefinitions(nullptr)
	{
		schema->constantDefinitions.push_back(this);
	}

	void ConstantCategoryDefinition::WriteEnter(std::ostream& output, const std::string& indent)
	{
		output << indent << "categoryDefinition " << source->GetName();
	}

	void ConstantCategoryDefinition::WriteBody(std::ostream& output, const std::string& indent)
	{
		SchemaEntityDefinition::WriteBody(output, indent);

		constantDefinitions.Write(output, indent);
	}
}
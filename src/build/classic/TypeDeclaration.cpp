#include "TypeDeclaration.h"

namespace JadeGit::Build::Classic
{
	void TypeDeclaration::WriteAttributes(std::ostream& output)
	{
		SchemaEntityDefinition::WriteAttributes(output);

		WriteAttribute(output, "final", source->final);
		WriteAttribute(output, "subschemaFinal", source->subschemaFinal);

		if (!source->persistentAllowed || !source->sharedTransientAllowed || !source->transientAllowed)
		{
			WriteAttribute(output, "persistentAllowed", source->persistentAllowed);
			WriteAttribute(output, "sharedTransientAllowed", source->sharedTransientAllowed);
			WriteAttribute(output, "transientAllowed", source->transientAllowed);
		}

		if (!source->subclassPersistentAllowed || !source->subclassSharedTransientAllowed || !source->subclassTransientAllowed)
		{
			WriteAttribute(output, "subclassPersistentAllowed", source->subclassPersistentAllowed);
			WriteAttribute(output, "subclassSharedTransientAllowed", source->subclassSharedTransientAllowed);
			WriteAttribute(output, "subclassTransientAllowed", source->subclassTransientAllowed);
		}
	}
}
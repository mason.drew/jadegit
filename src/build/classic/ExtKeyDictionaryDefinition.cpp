#include "ExtKeyDictionaryDefinition.h"
#include "SchemaDefinition.h"

using namespace JadeGit::Data;

namespace JadeGit::Build::Classic
{
	static NodeRegistration<ExtKeyDictionaryDefinition, CollClass> dict;
	static NodeRegistration<ExternalKeyDefinition, ExternalKey> key;

	ExtKeyDictionaryDefinition::ExtKeyDictionaryDefinition(SchemaDefinition* schema, const CollClass* klass) : DictionaryDefinition(schema, klass)
	{
		schema->externalKeyDefinitions.push_back(this);
	}

	void ExternalKeyDefinition::WriteBody(std::ostream& output, const std::string& indent)
	{
		output << indent << source->GetName() << ": " << source->type->GetName();

		if (source->length)
			output << "[" << source->length << "]";

		if (source->precision)
		{
			output << "[" << source->precision;
			if (source->scaleFactor)
				output << "," << source->scaleFactor;
			output << "]";
		}

		KeyDefinition::WriteBody(output, indent);
	}
}
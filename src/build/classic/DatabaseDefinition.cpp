#include "SchemaEntityDefinition.h"
#include "SchemaDefinition.h"
#include <jadegit/data/Database.h>
#include <jadegit/data/Class.h>

using namespace JadeGit::Data;

namespace JadeGit::Build::Classic
{
	class DatabaseDefinition : public SchemaEntityDefinition<Database, false, true>
	{
	public:
		using parent_node = SchemaDefinition;

		DefinitionNodes databaseFileDefinitions;
		DefinitionNodes classMapDefinitions;

		DatabaseDefinition(SchemaDefinition* schema, const Database* database) : SchemaEntityDefinition(database),
			databaseFileDefinitions("databaseFileDefinitions"),
			classMapDefinitions("classMapDefinitions")
		{
			schema->databaseDefinitions.push_back(this);
		}

	protected:
		void WriteEnter(std::ostream& output, const std::string& indent) override
		{
			output << source->GetName();
		}

		void WriteBody(std::ostream& output, const std::string& indent) override
		{
			SchemaEntityDefinition::WriteBody(output, indent);

			databaseFileDefinitions.Write(output, indent);

			if (source->defaultFile)
				output << indent << "defaultFileDefinition \"" << source->defaultFile->GetName() << "\";\n";

			classMapDefinitions.Write(output, indent);
		}
	};
	static NodeRegistration<DatabaseDefinition, Database> database;

	class DatabaseFileDefinition : public SchemaEntityDefinition<DbFile>
	{
	public:
		using parent_node = DatabaseDefinition;

		DatabaseFileDefinition(DatabaseDefinition* database, const DbFile* file) : SchemaEntityDefinition(file)
		{
			database->databaseFileDefinitions.push_back(this);
		}

	protected:
		void WriteEnter(std::ostream& output, const std::string& indent) override
		{
			output << indent << "\"" << source->GetName() << "\"";
		}
	};
	static NodeRegistration<DatabaseFileDefinition, DbFile> dbFile;

	class ClassMapDefinition : public DefinitionNode
	{
	public:
		using parent_node = DatabaseDefinition;

		ClassMapDefinition(DatabaseDefinition* database, const DbClassMap* classMap) : source(classMap)
		{
			database->classMapDefinitions.push_back(this);
		}

		void WriteBody(std::ostream& output, const std::string& indent) override
		{
			output << indent << source->diskClass->GetName() << " in \"" << source->diskFile->GetName() << "\"";
			
			switch (source->mode)
			{
			case DbClassMap::Mode::AllInstances:
				output << " allInstances";
				break;
			case DbClassMap::Mode::SubobjectInstances:
				output << " subobjectInstances";
				break;
			}	
			
			output << ";\n";
		}

	private:
		const DbClassMap* source;
	};
	static NodeRegistration<ClassMapDefinition, DbClassMap> dbClassMap;
}
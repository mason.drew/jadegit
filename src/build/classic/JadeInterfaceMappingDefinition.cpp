#include "JadeInterfaceMappingDefinition.h"
#include <jadegit/data/JadeInterface.h>
#include <jadegit/data/JadeImportedPackage.h>

namespace JadeGit::Build::Classic
{
	static NodeRegistration<JadeInterfaceMappingDefinition, Data::JadeInterfaceMapping> registrar;

	JadeInterfaceMappingDefinition::JadeInterfaceMappingDefinition(ClassDefinition* type, const Data::JadeInterfaceMapping* mapping) : SchemaDefinitionNode(&mapping->interface), source(mapping)
	{
		type->interfaceMappingDefinitions.push_back(this);
	}

	void JadeInterfaceMappingDefinition::WriteEnter(std::ostream& output, const std::string& indent)
	{
		output << indent;
		
		if (const Data::JadeImportedInterface* imported = dynamic_cast<const Data::JadeImportedInterface*>(SchemaDefinitionNode::source))
			output << imported->package->GetName() << "::";
		
		output << SchemaDefinitionNode::source->GetName();
	}

	void JadeInterfaceMappingDefinition::WriteBody(std::ostream& output, const std::string& indent)
	{
		SchemaDefinitionNode::WriteBody(output, indent);

		for (auto& mapping : source->methodMappings)
			output << indent << mapping.first << " is " << mapping.second->name << ";\n";
	}
}
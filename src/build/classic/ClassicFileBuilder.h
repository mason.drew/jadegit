#pragma once
#include <build/Builder.h>
#include "../FileBuilder.h"
#include "CommandFileWriter.h"
#include "SchemaFileWriter.h"
#include <jadegit/Version.h>

namespace JadeGit::Build::Classic
{
	class ClassicFileBuilder : public FileBuilder
	{
	public:
		ClassicFileBuilder(Builder &builder, const Version& jadeVersion);

	private:
		Version jadeVersion;
		Builder& builder;

		bool processing = false;
		bool reorging = false;
		bool latest = false;

		CommandFileWriter* commander = nullptr;
		SchemaFileWriter* extractor = nullptr;

		bool Visit(const BuildTask* task) override;
		bool Visit(const CommandTask* task) override;
		bool Visit(const ExtractTask* task) override;
		bool Visit(const ReorgTask* task) override;
		void Flush(bool final) override;
	};
}
#include "ClassicFileBuilder.h"

#include "..\BuildTask.h"
#include "..\CommandTask.h"
#include "..\ExtractTask.h"
#include "..\ReorgTask.h"

namespace JadeGit::Build::Classic
{
	ClassicFileBuilder::ClassicFileBuilder(Builder &builder, const Version& jadeVersion) :
		builder(builder),
		jadeVersion(jadeVersion)
	{}
	
	bool ClassicFileBuilder::Visit(const BuildTask* task)
	{
		return task->Execute(builder);
	}

	bool ClassicFileBuilder::Visit(const CommandTask* task)
	{
		if (processing && (!commander || !task->isLatest(latest)))
			return false;

		if (!commander)
		{
			processing = true;
			latest = task->isLatest();
			commander = new CommandFileWriter(builder, jadeVersion, std::bind(&ClassicFileBuilder::Flush, this, false), latest);
		}

		task->Execute(*commander);
		return true;
	}

	bool ClassicFileBuilder::Visit(const ExtractTask* task)
	{
		if (processing && (!extractor || !task->isLatest(latest)))
			return false;

		if (!extractor)
		{
			processing = true;
			latest = task->isLatest();
			extractor = new SchemaFileWriter(builder, latest);
		}

		return task->Execute(*extractor);
	}

	bool ClassicFileBuilder::Visit(const ReorgTask* task)
	{
		if (processing && !reorging)
			return false;

		if (task->Execute(builder))
		{
			processing = true;
			reorging = true;
			return true;
		}

		return false;
	}

	void ClassicFileBuilder::Flush(bool final)
	{
		if (commander)
		{
			delete commander;
			commander = nullptr;
		}

		if (extractor)
		{
			extractor->Write();
			delete extractor;
			extractor = nullptr;
		}

		processing = false;
		reorging = false;

		// Flush pending changes within deployment builder
		builder.Flush(final);
	}
}
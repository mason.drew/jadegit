#include "SchemaFileWriter.h"
#include "SchemaDefinition.h"
#include "TypeDeclaration.h"
#include <jadegit/data/Schema.h>

using namespace JadeGit::Data;

namespace JadeGit::Build::Classic
{
	SchemaFileWriter::SchemaFileWriter(Builder &builder, bool latest) : builder(builder), latest(latest) {}

	void SchemaFileWriter::Write()
	{
		root.Extract(builder, latest);
	}

	bool SchemaFileWriter::Declare(const Class* klass)
	{
		return (NodeFactory<TypeDeclaration>::Get().Create(std::type_index(typeid(*klass)), &root, klass, false));
	}

	bool SchemaFileWriter::Define(const Data::ActiveXLibrary* library, bool complete)
	{
		return (NodeFactory<DataNode>::Get().Create(std::type_index(typeid(*library)), &root, library, complete));
	}

	bool SchemaFileWriter::Define(const Application* application, bool complete)
	{
		return (NodeFactory<DataNode>::Get().Create(std::type_index(typeid(*application)), &root, application, complete));
	}

	bool SchemaFileWriter::Define(const JadeExposedClass* cls, bool complete)
	{
		return (NodeFactory<DefinitionNode>::Get().Create(std::type_index(typeid(*cls)), &root, cls, complete));
	}

	bool SchemaFileWriter::Define(const JadeExposedFeature* feature, bool complete)
	{
		return (NodeFactory<DefinitionNode>::Get().Create(std::type_index(typeid(*feature)), &root, feature, complete));
	}

	bool SchemaFileWriter::Define(const JadeExposedList* list, bool complete)
	{
		return (NodeFactory<DefinitionNode>::Get().Create(std::type_index(typeid(*list)), &root, list, complete));
	}

	bool SchemaFileWriter::Define(const JadeHTMLDocument* document, bool complete, const char* oldName)
	{
		return (NodeFactory<DataNode, const char*>::Get().Create(std::type_index(typeid(*document)), &root, document, complete, oldName));
	}

	bool SchemaFileWriter::Define(const JadeInterfaceMapping* mapping, bool complete)
	{
		return (NodeFactory<DefinitionNode>::Get().Create(std::type_index(typeid(*mapping)), &root, mapping, complete));
	}

	bool SchemaFileWriter::Define(const Form* form, bool complete)
	{
		return (NodeFactory<DataNode>::Get().Create(std::type_index(typeid(*form)), &root, form, complete));
	}

	bool SchemaFileWriter::Define(const RelationalAttribute* attribute, bool complete)
	{
		return (NodeFactory<DataNode>::Get().Create(std::type_index(typeid(*attribute)), &root, attribute, complete));
	}

	bool SchemaFileWriter::Define(const RelationalTable* table, bool complete)
	{
		return (NodeFactory<DataNode>::Get().Create(std::type_index(typeid(*table)), &root, table, complete));
	}

	bool SchemaFileWriter::Define(const RelationalView* view, bool complete)
	{
		return (NodeFactory<DataNode>::Get().Create(std::type_index(typeid(*view)), &root, view, complete));
	}

	bool SchemaFileWriter::Define(const Schema* schema, bool complete)
	{
		return (NodeFactory<DefinitionNode>::Get().Create(std::type_index(typeid(*schema)), &root, schema, complete));
	}

	bool SchemaFileWriter::Define(const SchemaEntity* entity, bool complete)
	{
		return (NodeFactory<DefinitionNode>::Get().Create(std::type_index(typeid(*entity)), &root, entity, complete));
	}
}
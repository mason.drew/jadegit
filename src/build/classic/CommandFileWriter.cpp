#include "CommandFileWriter.h"
#include <ostream>

using namespace std;

namespace JadeGit::Build::Classic
{
	CommandFileWriter::CommandFileWriter(Builder &builder, const Version& jadeVersion, function<void(void)> flush, bool latest) : flush(move(flush))
	{
		output = builder.AddCommandFile(latest);

		*output << "JadeCommandFile\n";
		*output << "JadeVersionNumber " << static_cast<string>(jadeVersion) << "\n";
		*output << "Commands\n";
	}

	CommandFileWriter::~CommandFileWriter()
	{
		*output << std::flush;
	}

	void CommandFileWriter::CreateLocale(const string& qualifiedName, const char* baseLocale)
	{
		*output << "Create Locale " << qualifiedName << " ";
		
		if (baseLocale)
			*output << "CloneOf " << baseLocale;
		else
			*output << "CopyFrom 0";
		
		*output << "\n";
	}

	void CommandFileWriter::Delete(const char* entityType, const string& qualifiedName)
	{
		/* Jade doesn't currently support deleting the following entities */
		// TODO: We could implement our own delete functions for these, however this approach wouldn't work for standard deployments
		string kind(entityType);
		if (kind == "Library")
		{
			*output << "// Delete " << entityType << " " << qualifiedName << " not supported\n";
		}
		else
			*output << "Delete " << entityType << " " << qualifiedName << "\n";
	}

	void CommandFileWriter::Rename(const char* entityType, const string& qualifiedName, const string& newName)
	{
		/* Jade doesn't currently support renaming the following entities, so we'll attempt to delete instead */
		// TODO: We could implement our own rename functions for these, however this approach wouldn't work for standard deployments
		string kind(entityType);
		if (kind == "Method" || kind == "Constant" || kind == "GlobalConstant" || kind == "GlobalConstantCategory")
		{
			*output << "// Rename to " << newName << " not supported\n";
			*output << "Delete " << entityType << " " << qualifiedName << "\n";
		}
		else
			*output << "Rename " << entityType << " " << qualifiedName << " " << newName << "\n";
	}

	void CommandFileWriter::ModifySchema(const string& schemaName, const string& key, const string& value)
	{
		*output << "Modify Schema " << schemaName << " " << key << " " << value << "\n";
	}

	void CommandFileWriter::MoveClass(const string& qualifiedName, const string& newSuperclass)
	{
		*output << "Move Class " << qualifiedName << " " << newSuperclass << "\n";
	}

	void CommandFileWriter::Flush()
	{
		this->flush();
	}
}
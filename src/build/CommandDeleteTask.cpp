#include "CommandDeleteTask.h"
#include "CommandStrategy.h"

namespace JadeGit::Build
{
	CommandDeleteTask::CommandDeleteTask(TaskGraph &graph, const char* entityType, std::string qualifiedName) : CommandTask(graph),
		entityType(entityType),
		qualifiedName(std::move(qualifiedName))
	{
	}

	void CommandDeleteTask::Execute(CommandStrategy &strategy) const
	{
		strategy.Delete(entityType, qualifiedName);
	}

	void CommandDeleteTask::Print(std::ostream& output) const
	{
		output << "Delete " << entityType << " " << qualifiedName;
	}
}
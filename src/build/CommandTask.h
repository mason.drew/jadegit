#pragma once
#include "FileTask.h"

namespace JadeGit::Build
{
	class CommandStrategy;

	class CommandTask : public FileTask
	{
	public:
		using FileTask::FileTask;

		bool Accept(TaskVisitor &v) const override final;
		int Priority() const override { return FileTask::Priority() * 2; }

		virtual void Execute(CommandStrategy &strategy) const = 0;
	};
}
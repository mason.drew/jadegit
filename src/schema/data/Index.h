#pragma once
#include <jade/JadeBytes.h>
#include <jadegit/git2.h>

namespace JadeGit::Schema
{
	class IndexData : public Jade::JadeBytes
	{
	public:
		Edition edition();
		std::unique_ptr<git_index> open() const;
		void read(git_index* index, bool force) const;
		void save(git_index* index) const;
	};
}
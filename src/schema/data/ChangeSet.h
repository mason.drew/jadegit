#include "Change.h"
#include <jadegit/vfs/FileSystem.h>
#include <jade/ObjectSet.h>
#include <jade/Transient.h>
#include <map>
#include <memory>

namespace JadeGit::Schema
{
	class GitChangeSet : public Jade::ObjectSet<GitChange>
	{
	public:
		using ObjectSet::ObjectSet;
		GitChangeSet();
		GitChangeSet(const GitEntity& entity);
		GitChangeSet(const WorktreeData& worktree);
		GitChangeSet(const GitChange& change, const Character* collection);

		bool stage(const FileSystem& fs, bool transient = false) const;

	protected:
		std::unique_ptr<Jade::Transient<GitChangeSet>> Clone() const;
		GitChange Clone(const GitChange& src, std::map<DskObjectId, DskObjectId>& directory) const;
		bool stage(Extract::Assembly& assembly) const;
	};
}

namespace Jade
{
	template<>
	Transient<JadeGit::Schema::GitChangeSet>::~Transient();
}
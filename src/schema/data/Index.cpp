#include "Index.h"
#include <jade/Exception.h>
#include <jade/Transaction.h>
#include <filesystem>
#include <Environment.h>

using namespace std;
using namespace Jade;

namespace JadeGit::Schema
{
	Edition IndexData::edition()
	{
		jade_throw(getBufferEdition());
		return oid.getEdition();
	}

	unique_ptr<git_index> IndexData::open() const
	{
		// Derive temporary index file name
		auto indexFile = makeTempDirectory() / std::format("index-[{}.{}.{}.{}.{}].tmp", oid.classId, oid.instId, oid.parClassId, oid.subLevel, oid.subId);

		// Ensure previous has been removed
		filesystem::remove(indexFile);

		// Extract index data to file
		extractToFile(indexFile.generic_string(), true);

		// Load index
		unique_ptr<git_index> index;
		git_throw(git_index_open(git_ptr(index), indexFile.generic_string().c_str()));
		return index;
	}

	void IndexData::read(git_index* index, bool force) const
	{
		// Extract index data to file
		extractToFile(git_index_path(index), true);

		// Read index
		git_throw(git_index_read(index, force));
	}

	void IndexData::save(git_index* index) const
	{
		// Handle copying in-memory index
		if (!git_index_path(index))
		{
			// Open index to copy to
			unique_ptr<git_index> dest = open();

			// Clear existing contents
			git_throw(git_index_clear(dest.get()));

			// Iterate source
			unique_ptr<git_index_iterator> iter;
			git_throw(git_index_iterator_new(git_ptr(iter), index));
			while (true)
			{
				const git_index_entry* entry = nullptr;
				auto error = git_index_iterator_next(&entry, iter.get());
				if (error == GIT_ITEROVER)
					break;
				git_throw(error);

				// Add entry to dest
				git_throw(git_index_add(dest.get(), entry));
			}

			// Save index
			save(dest.get());
		}
		else
		{
			// Write index to file
			git_throw(git_index_write(index));

			// Load into JadeBytes index
			loadFromFile(git_index_path(index));

			// Update parent edition to cause update notification
			DskObject parent;
			oid.getParent(parent.oid);
			if (!parent.isNull())
				jade_throw(parent.updateEdition());
		}
	}
}
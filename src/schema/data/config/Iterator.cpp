#include "Iterator.h"
#include "Backend.h"
#include "Entry.h"

namespace JadeGit::Schema::Backend
{
	template <typename T>
	static int wrapper(git_config_iterator* iter, T func)
	{
		try
		{
			return func(*static_cast<jadegit_config_iterator*>(iter));
		}
		catch (std::runtime_error& e)
		{
			git_error_set_str(GIT_ERROR_CONFIG, e.what());
		}

		return GIT_ERROR;
	}

	int jadegit_config_iterator__next(git_config_entry** out, git_config_iterator* iter)
	{
		return wrapper(iter, [&](jadegit_config_iterator& iter)
			{
				if (auto current = iter.next)
				{
					iter.next = current->next;
					*out = current;
					return GIT_OK;
				}

				return GIT_ITEROVER;
			});		
	}

	void jadegit_config_iterator__free(git_config_iterator* iter)
	{
		delete static_cast<jadegit_config_iterator*>(iter);
	}

	jadegit_config_iterator::jadegit_config_iterator(jadegit_config_backend& backend) : entries(backend.entries)
	{
		git_config_iterator::backend = &backend;
		git_config_iterator::next = &jadegit_config_iterator__next;
		git_config_iterator::free = &jadegit_config_iterator__free;

		// Set iterator to first entry
		next = entries->first;
	}
}
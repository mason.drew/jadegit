#pragma once
#include "Worktree.h"

namespace JadeGit::Schema
{
	class WorktreeState
	{
	public:
		static const WorktreeState& get(WorktreeData::State state);

		bool isActive() const;

		// Operations that may be invoked
		virtual void commit(const WorktreeData& worktree, const Repository& repo, const std::string& message) const;
		virtual bool load(const WorktreeData& worktree, IProgress* progress) const;
		virtual bool merge(const WorktreeData& worktree, const Repository& repo, const git_annotated_commit* theirs, IProgress* progress) const;
		virtual bool reset(const WorktreeData& worktree, const Repository& repo, const git_commit* commit, bool hard, IProgress* progress) const;
		virtual bool switch_(const WorktreeData& worktree, const LocalBranch& branch, IProgress* progress) const;
		virtual bool unload(const WorktreeData& worktree, IProgress* progress) const;

		// Handle aborting/continuing current operation
		virtual void abort(const WorktreeData& worktree, const Repository& repo) const;
		virtual bool continue_(const WorktreeData& worktree, const Repository& repo, const std::string& message, IProgress* progress) const;

		// Callback hook to update worktree when deployment is finished
		virtual void deployed(const WorktreeData& worktree, const WorktreeDeployment& deployment) const = 0;

		// Determine equivalent repository state
		virtual git_repository_state_t state(const WorktreeData& worktree) const = 0;

	protected:
		void set(const WorktreeData& worktree, WorktreeData::State state) const;

		void commit(git_oid& id, git_repository* repo, const char* update_ref, const std::string& message, git_index* index, size_t parent_count, const git_commit* parents[]) const;
	};
}
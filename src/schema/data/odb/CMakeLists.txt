target_sources(jadegit PRIVATE
	${CMAKE_CURRENT_LIST_DIR}/Backend.cpp
	${CMAKE_CURRENT_LIST_DIR}/Data.cpp
	${CMAKE_CURRENT_LIST_DIR}/Writepack.cpp
)
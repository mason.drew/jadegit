#include "Tree.h"
#include "Exception.h"
#include "ObjectRegistration.h"

using namespace std;
using namespace Jade;

namespace JadeGit::Schema
{
	static GitObjectRegistration<Tree> registration(TEXT("Tree"));

	Tree Tree::lookup(const Repository& repo, const git_oid& oid)
	{
		// Dereference transient collection
		auto trees = repo.getProperty<DskMemberKeyDictionary>(TEXT("trees"));

		// Return existing
		char id[GIT_OID_HEXSZ + 1] = "";
		Tree tree;
		jade_throw(trees.getAtKey(widen(git_oid_tostr(id, GIT_OID_HEXSZ + 1, &oid)).c_str(), tree));
		if (!tree.isNull())
			return tree;

		// Lookup tree
		unique_ptr<git_tree> ptr;
		git_throw(git_tree_lookup(git_ptr(ptr), repo, &oid));

		// Return new
		return Tree(repo, move(ptr));
	}

	Tree::Tree(const Repository& repo, unique_ptr<git_tree> ptr) : GitObject(registration, move(ptr))
	{
		setProperty(TEXT("id"), git_tree_id(static_cast<git_tree*>(*this)));
		jade_throw(setProperty(TEXT("parent"), repo));
	}

	int JOMAPI jadegit_tree_lookup(DskBuffer* pBuffer, DskParam* pParams, DskParam* pReturn)
	{
		return GitException::wrapper([&]()
			{
				DskParam* pRepo = nullptr;
				DskParam* pId = nullptr;
				JADE_RETURN(paramGetParameter(*pParams, 1, pRepo));
				JADE_RETURN(paramGetParameter(*pParams, 2, pId));

				Repository repo;
				JADE_RETURN(paramGetOid(*pRepo, repo.oid));

				string id;
				JADE_RETURN(paramGetString(*pId, id));

				git_oid oid = { 0 };
				git_throw(git_oid_fromstrn(&oid, id.c_str(), id.length()));

				auto tree = Tree::lookup(repo, oid);
				return paramSetOid(pReturn, tree.oid);
			});
	}
}
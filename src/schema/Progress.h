#pragma once
#include <jadegit/Progress.h>
#include <schema/Object.h>

namespace JadeGit::Schema
{
	class Progress : public Object, public IProgress
	{
	public:
		using Object::Object;

	private:
		void message(const std::string& message) override;
		void update(double progress, const char* caption) override;
		bool wasCancelled() override;
	};
}
#include "Commit.h"
#include "Committish.h"
#include "Repository.h"
#include "Exception.h"
#include "ObjectRegistration.h"

using namespace std;
using namespace Jade;

namespace JadeGit::Schema
{
	class RevisionWalker : public GitObject<git_revwalk>
	{
	public:
		using GitObject::GitObject;
		RevisionWalker(const Repository& repo);

		void hide(const ICommittish& committish) const
		{
			git_oid id = committish.id();
			git_throw(git_revwalk_hide(*this, &id));
		}

		void hideRef(const string& refname) const
		{
			git_throw(git_revwalk_hide_ref(*this, refname.c_str()));
		}

		Commit next() const
		{
			git_oid oid;
			int result = git_revwalk_next(&oid, *this);
			if (result == GIT_ITEROVER)
				return Commit();
			git_throw(result);

			return Commit::lookup(getProperty<Repository>(TEXT("repo")), oid);
		}

		void push(const ICommittish& committish) const
		{
			git_oid id = committish.id();
			git_throw(git_revwalk_push(*this, &id));
		}

		void pushRef(const string& refname) const
		{
			git_throw(git_revwalk_push_ref(*this, refname.c_str()));
		}
	};
	static GitObjectRegistration<RevisionWalker> registration(TEXT("RevisionWalker"));

	RevisionWalker::RevisionWalker(const Repository& repo) : GitObject(registration, nullptr)
	{
		jade_throw(setProperty(TEXT("repo"), repo));
		git_throw(git_revwalk_new(git_ptr(*this), repo));
	}

	int JOMAPI jadegit_revwalk_hide(DskBuffer* pBuffer, DskParam* pParams, DskParam* pReturn)
	{
		return GitException::wrapper([&]()
			{
				Committish committish;
				JADE_RETURN(paramGetOid(*pParams, committish.oid));

				RevisionWalker(pBuffer).hide(committish);
				return J_OK;
			});
	}

	int JOMAPI jadegit_revwalk_hide_ref(DskBuffer* pBuffer, DskParam* pParams, DskParam* pReturn)
	{
		return GitException::wrapper([&]()
			{
				string refname;
				JADE_RETURN(paramGetString(*pParams, refname));

				RevisionWalker(pBuffer).hideRef(refname);
				return J_OK;
			});
	}

	int JOMAPI jadegit_revwalk_new(DskBuffer* pBuffer, DskParam* pParams, DskParam* pReturn)
	{
		return GitException::wrapper([&]()
			{
				Repository repo;
				JADE_RETURN(paramGetOid(*pParams, repo.oid));

				RevisionWalker revwalk(repo);
				return paramSetOid(pReturn, revwalk.oid);
			});
	}

	int JOMAPI jadegit_revwalk_next(DskBuffer* pBuffer, DskParam* pParams, DskParam* pReturn)
	{
		return GitException::wrapper([&]()
			{
				auto commit = RevisionWalker(pBuffer).next();

				JADE_RETURN(paramSetOid(*pParams, commit.oid));
				return paramSetBoolean(*pReturn, !commit.isNull());
			});
	}

	int JOMAPI jadegit_revwalk_push(DskBuffer* pBuffer, DskParam* pParams, DskParam* pReturn)
	{
		return GitException::wrapper([&]()
			{
				Committish committish;
				JADE_RETURN(paramGetOid(*pParams, committish.oid));

				RevisionWalker(pBuffer).push(committish);
				return J_OK;
			});
	}

	int JOMAPI jadegit_revwalk_push_ref(DskBuffer* pBuffer, DskParam* pParams, DskParam* pReturn)
	{
		return GitException::wrapper([&]()
			{
				string refname;
				JADE_RETURN(paramGetString(*pParams, refname));

				RevisionWalker(pBuffer).pushRef(refname);
				return J_OK;
			});
	}

	int JOMAPI jadegit_revwalk_reset(DskBuffer* pBuffer, DskParam* pParams, DskParam* pReturn)
	{
		return GitException::wrapper([&]()
			{
				git_throw(git_revwalk_reset(RevisionWalker(pBuffer)));
				return J_OK;
			});
	}
}
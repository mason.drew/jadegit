#include "IndexEntry.h"
#include <schema/ObjectRegistration.h>

namespace JadeGit::Schema
{
	static GitObjectRegistration<IndexEntry> registration(TEXT("IndexEntry"));

	IndexEntry::IndexEntry(const Index& parent, const git_index_entry* entry) : GitObject(registration, entry, parent)
	{
		if (entry)
		{
			setProperty(TEXT("id"), &entry->id);
		}
	}

	int JOMAPI jadegit_index_entry_path(DskBuffer* pBuffer, DskParam* pParams, DskParam* pReturn)
	{
		return jadegit_proxy_mapping_string<IndexEntry>(pBuffer, pParams, &git_index_entry::path);
	}
}
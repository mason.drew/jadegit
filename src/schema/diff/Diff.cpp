#include "Diff.h"
#include "DiffCallbacks.h"
#include "DiffDelta.h"
#include <schema\Blob.h>
#include <schema\Tree.h>
#include <schema\ObjectRegistration.h>

using namespace std;
using namespace Jade;

namespace JadeGit::Schema
{
	static GitObjectRegistration<Diff> registration(TEXT("Diff"));

	Diff Diff::tree_to_tree(const Repository& repo, const Tree& old_tree, const Tree& new_tree)
	{
		unique_ptr<git_diff> ptr;
		git_throw(git_diff_tree_to_tree(git_ptr(ptr), repo, old_tree, new_tree, nullptr));
		return Diff(repo, move(ptr));
	}

	Diff::Diff(const Repository& repo, unique_ptr<git_diff> ptr) : GitObject(registration, move(ptr))
	{
		jade_throw(setProperty(TEXT("parent"), repo));

		auto sz = git_diff_num_deltas(*this);
		for (size_t idx = 0; idx < sz; idx++)
		{
			DiffDelta delta(*this, git_diff_get_delta(*this, idx));
		}
	}
	
	int JOMAPI jadegit_diff_blobs(DskBuffer* pBuffer, DskParam* pParams, DskParam* pReturn)
	{
		return GitException::wrapper([&]()
			{
				DskParam* pOldBlob = nullptr;
				DskParam* pOldPath = nullptr;
				DskParam* pNewBlob = nullptr;
				DskParam* pNewPath = nullptr;
				DskParam* pCallbacks = nullptr;
				JADE_RETURN(paramGetParameter(*pParams, 1, pOldBlob));
				JADE_RETURN(paramGetParameter(*pParams, 2, pOldPath));
				JADE_RETURN(paramGetParameter(*pParams, 3, pNewBlob));
				JADE_RETURN(paramGetParameter(*pParams, 4, pNewPath));
				JADE_RETURN(paramGetParameter(*pParams, 5, pCallbacks));

				Blob old_blob;
				JADE_RETURN(paramGetOid(*pOldBlob, old_blob.oid));

				string old_path;
				JADE_RETURN(paramGetString(*pOldPath, old_path));

				Blob new_blob;
				JADE_RETURN(paramGetOid(*pNewBlob, new_blob.oid));

				string new_path;
				JADE_RETURN(paramGetString(*pNewPath, new_path));

				DiffCallbacks callbacks;
				JADE_RETURN(paramGetOid(*pCallbacks, callbacks.oid));

				git_throw(git_diff_blobs(old_blob, old_path.c_str(), new_blob, new_path.c_str(), nullptr, callbacks, callbacks, callbacks, callbacks, &callbacks));
				return J_OK;
			});
	}

	int JOMAPI jadegit_diff_foreach(DskBuffer* pBuffer, DskParam* pParams, DskParam* pReturn)
	{
		return GitException::wrapper([&]()
			{
				Diff diff(pBuffer);

				DiffCallbacks callbacks;
				JADE_RETURN(paramGetOid(*pParams, callbacks.oid));

				git_throw(git_diff_foreach(diff, callbacks, callbacks, callbacks, callbacks, &callbacks));
				return J_OK;
			});
	}

	int JOMAPI jadegit_diff_tree_to_tree(DskBuffer* pBuffer, DskParam* pParams, DskParam* pReturn)
	{
		return GitException::wrapper([&]()
			{
				DskParam* pRepo = nullptr;
				DskParam* pOld = nullptr;
				DskParam* pNew = nullptr;
				JADE_RETURN(paramGetParameter(*pParams, 1, pRepo));
				JADE_RETURN(paramGetParameter(*pParams, 2, pOld));
				JADE_RETURN(paramGetParameter(*pParams, 3, pNew));

				Repository repo;
				JADE_RETURN(paramGetOid(*pRepo, repo.oid));

				Tree old_tree;
				JADE_RETURN(paramGetOid(*pOld, old_tree.oid));

				Tree new_tree;
				JADE_RETURN(paramGetOid(*pNew, new_tree.oid));

				auto diff = Diff::tree_to_tree(repo, old_tree, new_tree);
				return paramSetOid(pReturn, diff.oid);
			});
	}
}
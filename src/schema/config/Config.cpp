#include "Config.h"
#include "ConfigEntry.h"
#include <schema/Repository.h>
#include <schema/ObjectRegistration.h>

using namespace std;
using namespace Jade;

namespace JadeGit::Schema
{
	static GitObjectRegistration<Config> registration(TEXT("Config"));

	Config Config::default_()
	{
		unique_ptr<git_config> config;
		git_throw(git_config_open_default(git_ptr(config)));
		return Config(move(config));
	}

	Config::Config(const Repository& parent, unique_ptr<git_config> ptr) : GitObject(registration, move(ptr), parent)
	{
	}

	Config::Config(unique_ptr<git_config> ptr) : Config(Repository(), move(ptr))
	{
	}

	ConfigEntry Config::get_entry(const string& name) const
	{
		git_config_entry* ptr = nullptr;
		git_throw(git_config_get_entry(&ptr, *this, name.c_str()));
		return ConfigEntry(*this, ptr);
	}

	Config Config::snapshot() const
	{
		unique_ptr<git_config> snapshot;
		git_throw(git_config_snapshot(git_ptr(snapshot), *this));
		return Config(move(snapshot));
	}

	int JOMAPI jadegit_config_default(DskBuffer* pBuffer, DskParam* pParams, DskParam* pReturn)
	{
		return GitException::wrapper([&]()
			{
				return paramSetOid(pReturn, Config::default_().oid);
			});
	}

	int JOMAPI jadegit_config_get_entry(DskBuffer* pBuffer, DskParam* pParams, DskParam* pReturn)
	{
		return GitException::wrapper([&]()
			{
				string name;
				jade_throw(paramGetString(*pParams, name));

				return paramSetOid(pReturn, Config(pBuffer).get_entry(name).oid);
			});
	}

	int JOMAPI jadegit_config_snapshot(DskBuffer* pBuffer, DskParam* pParams, DskParam* pReturn)
	{
		return GitException::wrapper([&]()
			{
				return paramSetOid(pReturn, Config(pBuffer).snapshot().oid);
			});
	}
}
#pragma once
#include "Object.h"
#include <functional>
#include <future>

namespace JadeGit
{
	class IProgress;
}

namespace JadeGit::Schema
{
	class Task : public Object
	{
	public:
		static Task* getThis(DskBuffer* buffer)
		{
			return static_cast<Task*>(Object::getThis(buffer));
		}

		typedef std::function<bool(IProgress*)> Function;
		static int make(DskParam* pReturn, Function function);
		

		Task(const DskObjectId& pOid);
		~Task();

		void run();
		bool get();

		bool execute(IProgress* progress);

	protected:
		Task(Function function);

	private:
		std::packaged_task<bool(IProgress*)> task;
		std::future<bool> future;
	};
}
#include "Global.h"
#include <jade/AppContext.h>
#include <jade/Exception.h>
#include <jade/String.h>
#include <jade/Transaction.h>
#include <jomobj.hpp>
#include <future>
#include <thread>

using namespace std;
using namespace Jade;

namespace JadeGit::Schema::Install
{
	DskObject getGlobal()
	{
		// Get schema
		DskSchema rootSchema(&RootSchemaOid);
		DskSchema schema;
		rootSchema.getSubschema(TEXT("JadeGitSchema"), schema);

		// Check schema exists
		if (schema.isNull())
			return DskObject();

		// Get global class
		DskClass globalCls;
		schema.getSchemaClass(TEXT("GitGlobal"), globalCls);

		// Check class exists
		if (globalCls.isNull())
			return DskObject();

		// Get first instance
		DskObject global;
		globalCls.firstInstance(&global.oid);
		return global;
	}

	string GitGlobal::getRevision()
	{
		// Get global
		auto global = getGlobal();
		if (global.isNull())
			return string();

		// Get node
		DskHandle node = { 0 };
		jade_throw(jomGetNodeContextHandle(nullptr, &node));

		// Get current commit via background thread (to avoid keeping class in use error preventing schema update)
		packaged_task task([&]
		{
			AppContext app(&node, TEXT("JadeGitSchema"), TEXT("JadeGitBackground"));

			DskParamString param;
			global.getProperty(TEXT("revision"), &param);

			string result;
			paramGetString(param, result);
			return result;
		});

		future f = task.get_future();
		thread(move(task)).join();
		return f.get();
	}

	void GitGlobal::setRevision(string commit)
	{
		// Get global
		auto global = getGlobal();
		
		// Set current commit
		Transaction transaction;
		DskParamString param(commit);
		jade_throw(global.setProperty(TEXT("revision"), &param));
		transaction.commit();
	}
}
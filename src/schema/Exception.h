#pragma once
#include "Object.h"

namespace JadeGit::Schema
{
	class GitException : public Object
	{
	public:
		GitException(const char* message);
		GitException(std::runtime_error &ex, ExceptionCause cause = EC_PRECONDITION);
		~GitException() {}

        template <typename T>
        static int wrapper(T func)
        {
			try
			{
				return func();
			}
			catch (std::runtime_error& e)
			{
				GitException ex(e);
			}

            return 0;
        }
	};
}
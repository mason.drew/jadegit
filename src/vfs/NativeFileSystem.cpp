#include <jadegit/vfs/NativeFileSystem.h>
#include <jadegit/vfs/File.h>
#include <jadegit/Exception.h>
#include "NativeFile.h"

namespace fs = std::filesystem;

namespace JadeGit
{
	std::time_t convertTime(const std::string& s)
	{
		std::tm tm = {};
		std::stringstream ss(s);
		ss >> std::get_time(&tm, "%d/%m/%y %H:%M:%S");
		return std::mktime(&tm);
	}

	NativeFileSystem::NativeFileSystem(const fs::path& base, bool writable, const std::string& author, std::time_t modified) : base(base), writable(writable), author(author), modified(modified)
	{
		// Check if base path already exists
		if (fs::exists(base))
		{
			// Ensure base path is a directory
			if (!fs::is_directory(base))
				throw jadegit_exception(base.generic_string() + " is not a directory");
		}
		else
		{
			// Create directory if allowed
			if (writable)
				fs::create_directories(base);
			else
				throw jadegit_exception(base.generic_string() + " doesn't exist");
		}
	}

	NativeFileSystem::NativeFileSystem(const fs::path& base, bool writable, const std::string& author, const std::string& modified) : NativeFileSystem(base, writable, author, convertTime(modified)) {}

	NativeFileSystem::~NativeFileSystem() {}

	bool NativeFileSystem::isReadOnly() const
	{
		return !writable;
	}

	File NativeFileSystem::open(const fs::path& path) const
	{
		return File(std::make_unique<NativeFile>(*this, path));
	}
}
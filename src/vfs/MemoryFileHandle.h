#pragma once
#include <jadegit/vfs/AnyFile.h>

namespace JadeGit
{
	class MemoryFileNode;
	class MemoryFileSystem;

	class MemoryFileHandle : public AnyFile
	{
	public:
		MemoryFileHandle(const MemoryFileSystem& fs, const std::filesystem::path& path);
		~MemoryFileHandle();

		std::unique_ptr<AnyFile> clone() const override;
		const FileSystem* vfs() const override;
		std::filesystem::path path() const override;
		bool exists() const override;
		bool isDirectory() const override;
		void signature(std::string& author, std::time_t& modified, size_t lineFrom, size_t lineTo) const override {}
		std::unique_ptr<AnyFileIterator> begin() const override;

		void createDirectory() override;
		void remove() override;

		std::unique_ptr<std::istream> createInputStream(std::ios_base::openmode mode) const override;
		std::unique_ptr<std::ostream> createOutputStream(std::ios_base::openmode mode) override;

	protected:
		const MemoryFileSystem& fs;
		std::filesystem::path relative_path;
		MemoryFileNode* file = nullptr;
	};
}
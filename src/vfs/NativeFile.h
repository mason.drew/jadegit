#pragma once
#include <jadegit/vfs/AnyFile.h>

namespace JadeGit
{
	class NativeFileSystem;

	class NativeFile : public AnyFile
	{
	public:
		NativeFile(const NativeFileSystem& fs, const std::filesystem::path& path);
		~NativeFile();

		std::unique_ptr<AnyFile> clone() const override;
		const FileSystem* vfs() const override;
		std::filesystem::path path() const override;
		bool exists() const override;
		bool isDirectory() const override;
		void signature(std::string& author, std::time_t& modified, size_t lineFrom, size_t lineTo) const override;
		std::unique_ptr<AnyFileIterator> begin() const override;
		void createDirectory() override;
		void copy(AnyFile& dest) const override;
		void move(AnyFile& dest) override;
		void rename(const std::filesystem::path& filename) override;
		void remove() override;
		std::unique_ptr<std::istream> createInputStream(std::ios_base::openmode mode) const override;
		std::unique_ptr<std::ostream> createOutputStream(std::ios_base::openmode mode) override;

	protected:
		const NativeFileSystem& fs;
		std::filesystem::path relative_path;
	};
}
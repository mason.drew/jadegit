#include "GitFileBlame.h"
#include <chrono>
#include <assert.h>

using namespace std;
using namespace std::chrono;

namespace JadeGit
{
	GitFileBlame::GitFileBlame(unique_ptr<git_blame> blame) : blame(move(blame)) {}

	GitFileBlame::GitFileBlame(std::string author, std::time_t modified) : author(move(author)), modified(move(modified)) {}

	void GitFileBlame::signature(std::string& author, std::time_t& modified, size_t lineFrom, size_t lineTo) const
	{
		if (!blame)
		{
			author = this->author;
			modified = this->modified;
			return;
		}

		const git_blame_hunk* result = nullptr;
		const git_blame_hunk* hunk = nullptr;

		for (auto line = lineFrom; line <= lineTo; line += hunk->lines_in_hunk)
		{
			if (hunk = git_blame_get_hunk_byline(blame.get(), line))
			{
				if (!result || hunk->final_signature->when.time > result->final_signature->when.time)
					result = hunk;
			}
			else
				break;
		}

		assert(result);

		if (result)
		{
			char buffer[9] = "";
			author = std::string(result->final_signature->name) + " (" + git_oid_tostr(buffer, 9, &result->final_commit_id) + ")";
			modified = result->final_signature->when.time;
		}
	}
}
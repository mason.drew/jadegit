#pragma once
#include <jadegit/git2.h>
#include <ctime>

namespace JadeGit
{
	class GitFileBlame
	{
	public:
		GitFileBlame(std::unique_ptr<git_blame> blame);
		GitFileBlame(std::string author, std::time_t modified);

		void signature(std::string& author, std::time_t& modified, size_t lineFrom, size_t lineTo) const;

	private:
		std::unique_ptr<git_blame> blame;
		std::string author;
		std::time_t modified;
	};
}
#include "MemoryFileHandle.h"
#include "MemoryFile.h"
#include "MemoryFileIterator.h"
#include "MemoryFileStream.h"
#include <jadegit/vfs/MemoryFileSystem.h>
#include <jadegit/Exception.h>

namespace JadeGit
{
	MemoryFileHandle::MemoryFileHandle(const MemoryFileSystem& fs, const std::filesystem::path& path) : fs(fs), relative_path(path)
	{
		file = path.empty() ? fs.root : fs.root->find(path);
	}

	MemoryFileHandle::~MemoryFileHandle() {}

	std::unique_ptr<AnyFile> MemoryFileHandle::clone() const
	{
		return std::make_unique<MemoryFileHandle>(fs, relative_path);
	}

	const FileSystem* MemoryFileHandle::vfs() const
	{
		return &fs;
	}

	std::filesystem::path MemoryFileHandle::path() const
	{
		return relative_path;
	}

	bool MemoryFileHandle::exists() const
	{
		return !!file;
	}

	bool MemoryFileHandle::isDirectory() const
	{
		return file && file->isDirectory();
	}

	std::unique_ptr<AnyFileIterator> MemoryFileHandle::begin() const
	{
		return isDirectory() ? std::make_unique<MemoryFileIterator>(clone(), *static_cast<MemoryFileDirectory*>(file)) : nullptr;
	}

	void MemoryFileHandle::createDirectory()
	{
		throw jadegit_unimplemented_feature();
	}

	void MemoryFileHandle::remove()
	{
		if (!file)
			throw jadegit_exception("File does not exist");

		file->remove();
		file = nullptr;
	}

	std::unique_ptr<std::istream> MemoryFileHandle::createInputStream(std::ios_base::openmode mode) const
	{
		/* Check file already exists */
		if (!file)
			throw jadegit_exception("File does not exist");

		/* Check file is not a directory */
		if (file->isDirectory())
			throw jadegit_exception("Cannot create input stream for directory");

		return std::make_unique<MemoryFileStream>(*static_cast<MemoryFile*>(file), mode);
	}

	std::unique_ptr<std::ostream> MemoryFileHandle::createOutputStream(std::ios_base::openmode mode)
	{
		/* Make the file if we have to */
		if (!file)
			file = fs.root->make(relative_path);

		return std::make_unique<MemoryFileStream>(*static_cast<MemoryFile*>(file), mode);
	}
}
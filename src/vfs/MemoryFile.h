#pragma once
#include <map>
#include <string>
#include <filesystem>

namespace JadeGit
{
	class MemoryFileDirectory;
	class MemoryFileSystem;

	class MemoryFileNode
	{
	public:
		MemoryFileNode(MemoryFileDirectory* parent, const std::string& name);

		std::filesystem::path path() const;

		void remove();
		virtual bool isDirectory() const = 0;

	protected:
		friend MemoryFileDirectory;
		virtual ~MemoryFileNode();
		MemoryFileNode();

		MemoryFileDirectory* parent = nullptr;

	private:
		std::string name;
	};

	class MemoryFileBuffer;

	class MemoryFile : public MemoryFileNode
	{
		friend MemoryFileBuffer;
		friend MemoryFileDirectory;

	protected:
		using MemoryFileNode::MemoryFileNode;

		std::string content;

		bool isDirectory() const override {	return false; };
	};

	class MemoryFileDirectory : public MemoryFileNode
	{
	public:
		MemoryFileDirectory(MemoryFileDirectory* parent, const std::string& name);

		MemoryFileNode* find(const std::filesystem::path& path) const;
		MemoryFile* make(const std::filesystem::path& path);

		std::map<std::string, MemoryFileNode*>::const_iterator begin() const;
		std::map<std::string, MemoryFileNode*>::const_iterator end() const;

	protected:
		// Root directory constructor
		friend MemoryFileSystem;
		MemoryFileDirectory();
		~MemoryFileDirectory();

		friend MemoryFileNode;
		std::map<std::string, MemoryFileNode*> contents;

		bool isDirectory() const override { return true; };

		MemoryFileDirectory* make_directory(const std::filesystem::path& path);
	};
}
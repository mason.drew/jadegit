#pragma once
#include <jadegit/vfs/AnyFileIterator.h>

namespace JadeGit
{
	class GitFileSystem;

	class GitFileIterator : public AnyFileIterator
	{
	public:
		GitFileIterator(GitFileSystem& fs, const std::filesystem::path& path);
		~GitFileIterator();

		std::unique_ptr<AnyFileIterator> clone() const override;
		bool valid() const override;
		std::filesystem::path filename() const override;
		void next() override;

	protected:
		GitFileSystem& fs;
	};
}
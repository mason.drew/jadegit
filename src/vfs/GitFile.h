#pragma once
#include <jadegit/vfs/AnyFile.h>
#include <jadegit/git2.h>

namespace JadeGit
{
	class GitFileBlame;
	class GitFileSystem;

	class GitFile : public AnyFile
	{
	public:
		GitFile(const GitFileSystem& fs, const std::filesystem::path& path);
		GitFile(const GitFileSystem& fs, const std::filesystem::path& path, const git_oid& id);
		~GitFile();

		std::unique_ptr<AnyFile> clone() const override;
		const FileSystem* vfs() const override;
		std::filesystem::path path() const override;
		bool exists() const override;
		bool isDirectory() const override;
		void signature(std::string& author, std::time_t& modified, size_t lineFrom, size_t lineTo) const override;
		std::unique_ptr<AnyFileIterator> begin() const override;
		void createDirectory() override;
		void remove() override;

		std::unique_ptr<std::istream> createInputStream(std::ios_base::openmode mode) const override;
		std::unique_ptr<std::ostream> createOutputStream(std::ios_base::openmode mode) override;

	protected:
		const GitFileSystem& fs;
		std::filesystem::path relative_path;
		git_oid id;

		mutable std::unique_ptr<GitFileBlame> blame;

	private:
		git_oid find(const GitFileSystem& fs, const std::filesystem::path& relative_path) const;
	};
}
#pragma once
#include <jadegit/Exception.h>
#include <map>
#include <assert.h>

namespace JadeGit
{
	template <class TBase, typename TKey, typename... Args>
	class Factory
	{
	public:
		static Factory &Get() { static Factory f; return f; }

		class Registration
		{
		public:
			virtual TBase* Create(Args... args) const = 0;
		};

		void Register(const TKey key, const Registration* registrar) { registry[key] = registrar; }

		TBase* Create(const TKey key, Args... args){
			auto iter = registry.find(key);
			if(iter != registry.end())
				return iter->second->Create(args...);

			throw jadegit_exception("Missing factory registration");
		}

		TBase* TryCreate(const TKey key, Args... args) {
			auto iter = registry.find(key);
			if (iter != registry.end())
				return iter->second->Create(args...);
			else
				return nullptr;
		}
	
	protected:
		Factory() {}

	private:
		typedef std::map<TKey, const Registration*> Registry;
		Registry registry;
	};

	template <class TBase, class TDerived, typename TKey, typename... Args>
	class FactoryRegistration : Factory<TBase, TKey, Args...>::Registration
	{
	public:
		TBase* Create(Args... args) const override
		{
			return new TDerived(args...);
		}

		FactoryRegistration(const TKey key)
		{
			Factory<TBase, TKey, Args...>::Get().Register(key, this);
		}
	};
}
#pragma once
#include <filesystem>

namespace JadeGit
{
	using namespace std::filesystem;

	path getHomeDirectory();
	path getLogDirectory();

	path makeTempDirectory();
	path makeTempDirectory(const path& subfolder);

	void setLogDirectory(const path& path);
}
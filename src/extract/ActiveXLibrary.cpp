#include "Type.h"
#include "DataMapper.h"
#include "EntityRegistration.h"

using namespace JadeGit::Data;

namespace JadeGit::Extract
{
	class ActiveXLibrary : public Entity
	{
	public:
		using Entity::Entity;

		std::string getName() const override
		{
			// Return name for base class, resolved via ActiveXClass
			return getProperty<Object>(PRP_ActiveXLibrary_activeXClass).getProperty<Class>(PRP_ActiveXClass_baseClass).getName();
		}

	protected:
		bool lookup(const Entity* ancestor, const QualifiedName& path) override
		{
			// Resolve base class
			Class baseClass;
			if (!baseClass.find(ancestor, &path, false))
				return false;

			// Dereference ActiveXClass
			auto activeXClass = baseClass.getProperty<Object>(PRP_Class_activeXClass);
			if (activeXClass.isNull())
				return false;

			// Dereference ActiveXLibrary
			oid = activeXClass.getProperty<DskObjectId>(PRP_ActiveXClass_activeXLibrary);
			return !oid.isNull();
		}

		DskObjectId GetParentId() const override
		{
			return getProperty<DskObjectId>(PRP_ActiveXLibrary__schema);
		}
	};
	static EntityRegistration<ActiveXLibrary> activeXLibrary(DSKACTIVEXLIBRARY);

	class ActiveXClass : public Object
	{
	public:
		using Object::Object;

	protected:
		Data::Object* resolve(Data::Assembly& assembly, Data::Component* parent, bool shallow) const override
		{
			// Resolve existing
			if (Data::Class* baseClass = dynamic_cast<Data::Class*>(parent))
				if (baseClass->activeXClass)
					return baseClass->activeXClass;

			// Default to creating new instance
			return Object::resolve(assembly, parent, shallow);
		}
	};
	static ObjectRegistration<ActiveXClass> activeXClass(DSKACTIVEXCLASS);

	class ActiveXFeature : public Object
	{
	public:
		using Object::Object;

	protected:
		Data::Object* resolve(Data::Assembly& assembly, Data::Component* parent, bool shallow) const override
		{
			// Resolve existing
			if (Data::Feature* feature = dynamic_cast<Data::Feature*>(parent))
				if (feature->activeXFeature)
					return feature->activeXFeature;

			// Default to creating new instance
			return Object::resolve(assembly, parent, shallow);
		}
	};
	static ObjectRegistration<ActiveXFeature> activeXFeature(DSKACTIVEXFEATURE);
}
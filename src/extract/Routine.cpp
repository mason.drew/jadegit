#include "Routine.h"
#include "Type.h"
#include "DataMapper.h"
#include "ObjectRegistration.h"
#include <jadegit/data/Routine.h>
#include <jadegit/data/ScriptElement.h>
#include <jadegit/data/RootSchema/RoutineMeta.h>
#include <jadegit/data/RootSchema/ParameterMeta.h>
#include <jadegit/data/RootSchema/ReturnTypeMeta.h>

using namespace JadeGit::Data;

namespace JadeGit::Extract
{
	class TypeUsage : public Object
	{
	public:
		TypeUsage(DskObjectId target) : Object(target) {}

		DskObjectId GetParentId() const
		{
			DskObjectId oid;
			jade_throw(getProperty(PRP_ScriptElement_schemaScript, &oid));
			return oid;
		}
	};

	class Parameter : public TypeUsage
	{
	public:
		Parameter(DskObjectId target) : TypeUsage(target) {}
	};

	class ReturnType : public TypeUsage
	{
	public:
		ReturnType(DskObjectId target) : TypeUsage(target) {}

	protected:
		Data::Object* resolve(Data::Assembly& assembly, Data::Component* parent, bool shallow) const override
		{
			// Resolve existing return type
			if (Data::Routine* routine = dynamic_cast<Data::Routine*>(parent))
				if (routine->returnType)
					return routine->returnType;

			// Default to creating new instance
			return TypeUsage::resolve(assembly, parent, shallow);
		}
	};

	// Returns true if type usage has a specific length which isn't predefined by type
	bool hasSpecificLength(const Object& usage)
	{
		DskType type = usage.getProperty<DskType>(PRP_TypeUsage_type);
		return !type.isNull() && !hasPredefinedLength(type);
	}

	static DataMapper<RoutineMeta> routineMapper(DSKROUTINE, &RootSchema::routine, {
		{PRP_Routine_executionLocation, new DataProperty(&RoutineMeta::executionLocation)},
		{PRP_Routine_featureUsages, nullptr},
		{PRP_Routine_keyUsages, nullptr},
		{PRP_Routine_parameters, new DataProperty<RoutineMeta>(nullptr)},
		{PRP_Routine_returnType, new DataProperty<RoutineMeta>(nullptr)}
		});

	static DataMapper<TypeUsageMeta> typeUsageMapper(DSKTYPEUSAGE, &RootSchema::typeUsage, {
		{PRP_TypeUsage_name, new DataProperty(&TypeUsageMeta::name)},
		{PRP_TypeUsage_type, new DataProperty(&TypeUsageMeta::type)}
		});

	static DataMapper<ParameterMeta> parameterMapper(DSKPARAMETER, &RootSchema::parameter, {
		{PRP_Parameter__wsdlName, nullptr},
		{PRP_Parameter_length, new DataProperty(&ParameterMeta::length, [](const DskObject& object) { return hasSpecificLength(Object(&object.oid)); })},		// Extract length provided it's not predefined
		{PRP_Parameter_usage, new DataProperty(&ParameterMeta::usage)}
		});

	static DataMapper<ReturnTypeMeta> returnTypeMapper(DSKRETURNTYPE, &RootSchema::returnType, {
		{PRP_ReturnType_length, new DataProperty(&ReturnTypeMeta::length, [](const DskObject& object) { return hasSpecificLength(Object(&object.oid)); })}	// Extract length provided it's not predefined
		});

	static ObjectRegistration<Parameter> parameter(DSKPARAMETER);
	static ObjectRegistration<ReturnType> returnType(DSKRETURNTYPE);
}
#pragma once
#include "CommandRegistry.h"
#include <string>

namespace JadeGit::Console
{
	template <class TCommand>
	class CommandRegistration : CommandRegistry::Registration
	{
	public:
		CommandRegistration(std::string name = "", std::string description = "") : name(move(name)), description(move(description)) {}

	protected:
		void setup(CLI::App& app, Session& context) const override final
		{
			auto cmd = app.add_subcommand(name, description);
			auto opt = std::make_shared<TCommand>(*cmd, context);
			cmd->callback([opt]() { opt->execute(); });
		}

	private:
		std::string name;
		std::string description;
	};
}
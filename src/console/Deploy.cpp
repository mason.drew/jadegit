#include "Command.h"
#include "CommandRegistration.h"
#include <deploy/Manifest.h>

using namespace std;
using namespace JadeGit;
using namespace JadeGit::Deploy;

namespace JadeGit::Console
{
	class Deploy : public Command
	{
	public:
		Deploy(CLI::App& cmd, Session& session) : Command(cmd, session)
		{
			// Need at least one subcommand
			cmd.require_subcommand(1);

			// Setup manifest subcommands
			{
				auto manifest = cmd.add_subcommand("manifest", "Deploy manifest commands")->require_subcommand(1);

				{
					auto get = manifest->add_subcommand("get", "Retrieve manifest")->require_subcommand();
					get->add_subcommand("origin", "Display origin")->callback([this]() { cout << Manifest::get().origin << endl; });
					get->add_subcommand("commit", "Display commit")->callback([this]() { cout << Manifest::get().commit << endl; });
				}

				{
					auto set = manifest->add_subcommand("set", "Update manifest")->require_option();
					set->add_option("-o,--origin", this->origin, "Source repository")->capture_default_str();
					set->add_option("-c,--commit", this->commit, "Commit ID")->capture_default_str();
					set->callback([this]() { Manifest::set(this->origin, this->commit); });
				}
			}

			// TODO: Setup subcommands for deploy start/finish
		}

		void execute() final
		{
			// Not applicable
		}

	protected:
		string origin;
		string commit;
	};
	static CommandRegistration<Deploy> registration("deploy", "Deployment commands");
}
#include <jadegit/Version.h>
#include <format>

using namespace std;

namespace JadeGit
{
	Version::Version(const char* version) : Version()
	{
		sscanf_s(version, "%d.%d.%d.%d", &major, &minor, &revision, &build);
	}

	Version::Version(const string& version) : Version(version.c_str())
	{
	}

	Version::operator string() const
	{
		if (empty())
			return "99.0.00";

		if (build)
			return format("{}.{}.{}.{}", major, minor, revision, build);

		return format("{}.{}.{:0>2}", major, minor, revision);
	}

	bool Version::empty() const
	{
		return !major && !minor && !revision && !build;
	}
}
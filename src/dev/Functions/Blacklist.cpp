#include "Function.h"
#include <Log.h>

namespace JadeGit::Development
{
	// Rejects blacklisted functions (not applicable/needed)
	class BlacklistFunction : public Function
	{
	public:
		BlacklistFunction() : Function
		({
			// Patch related functions
			Function::AccessToPatchFns,
			Function::AddPatch,
			Function::BrowsePatches,
			Function::BrowsePatchSummary,
			Function::ChangePatch,
			Function::ClosePatch,
			Function::ExtractPatch,
			Function::MovePatchDetail,
			Function::RecreatePatchVersion,
			Function::RefreshPatchList,
			Function::ReopenPatch,
			Function::SetPatch,
			Function::SetPatchNumber,
			Function::SetPatchVersionEnabled,
			Function::SetPatchVersionNumber,
			Function::ShowAllPatchedMethods,
			Function::ShowExtractPatch,
			Function::ShowMethodHistory,
			Function::ShowPatchedMethods,
			Function::ShowPatchSummary,
			Function::ShowRemovePatchHistory,
			Function::ShowTypeHistory,
			Function::UnsetPatch,
			Function::ViewAllPatches,
			Function::ViewClosedPatches,
			Function::ViewCurrentUserPatches,
			Function::ViewOpenPatches,

			// Delta related functions
			Function::AddDelta,
			Function::BrowseDeltas,
			Function::CheckInAllInDelta,
			Function::CloseDelta,
			Function::SetDelta,
			Function::ShowMethodsInDelta,
			Function::UncheckOutAllInDelta,
			Function::UnsetDelta
		}) {};

	protected:
		bool execute(const std::string& entityName) const override
		{
			LOG_TRACE("blacklisted");
			return false;
		}
	};
	static BlacklistFunction blacklist;
}
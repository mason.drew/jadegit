#include "Development.h"

namespace JadeGit::Development
{
	/*
		jadegitdev currently forwards calls from JADE to jadegitscm which implements each hook.

		Ideally, jadegitdev would contain the implementation (long-term goal), but first need to overcome issues
		exporting relevant classes from jadegitscm.  Ultimately, this may prove to be easier with C++ modules.
	*/


	/*******************************************************************************

	Description:  	jadeDevelopmentUserInfo hook

	Called once when the user attempts to sign on (can be called again if the previous signon was rejected)

	Parameters      userName  - the user sign on id
					password  - the password entered on the Jade Sign on screen

	*******************************************************************************/
	int JOMAPI jadeDevelopmentUserInfo(const Character* pUserName, const Character* pPassword)
	{
		return jadeUserInfo(pUserName, pPassword);
	}

	/************************************************************************************

	Description:  	jadeDevelopmentFunctionSelected hook

	Called for every function performed within the JADE development environment.

	Parameters      userName  - the user sign on id
					taskName  - the task about to be performed
					entityName - the entity that the task is to performed on

	**************************************************************************************/
	int JOMAPI jadeDevelopmentFunctionSelected(const Character* pUserName, const Character* pTaskName, const Character* pEntityName)
	{
		return jadeFunctionSelected(pUserName, pTaskName, pEntityName);
	}

	/************************************************************************************

	Description:  	jadeDevelopmentPatchControl

	Called for every function performed within the JADE development environment.

	Parameters      userName		- the user sign on id
					patchDetails	- <entity-patch-number>:<user-patch-number>:<status>
					entityName		- the entity that the task is to performed on
					entityType		- class of the object being modified.
					operation		- type of operation (Add, Update, or Delete)

	**************************************************************************************/
	int JOMAPI jadeDevelopmentPatchControl(const Character* pUserName, const Character* pPatchDetails, const Character* pEntityName, const Character* pEntityType, const Character* pOperation)
	{
		return jadePatchControl(pUserName, pPatchDetails, pEntityName, pEntityType, pOperation);
	}
}
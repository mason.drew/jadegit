#pragma once
#include <jadegit/Progress.h>
#include <git2/repository.h>
#include <memory>

namespace JadeGit::Deploy
{
	class DeploymentBuilder;

	class Director
	{
	public:
		Director(git_repository& repo, DeploymentBuilder& builder, IProgress* progress = nullptr);
		~Director();

		void build(std::string revision) const;
		void build(std::string from, std::string to) const;

	private:
		class Impl;
		std::unique_ptr<Impl> impl;
	};
}
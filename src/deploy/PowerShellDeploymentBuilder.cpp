#include "PowerShellDeploymentBuilder.h"
#include <jadegit/vfs/File.h>
#include <jadegit/Exception.h>
#include <format>

using namespace std;

namespace JadeGit::Deploy
{
	PowerShellDeploymentBuilder::PowerShellDeploymentBuilder(const FileSystem& fs) : fs(fs)
	{
	}

	void PowerShellDeploymentBuilder::start(const string& origin, const string& currentCommit, const string& latestCommit)
	{
		// Reset working variables
		files = 0;
		lastschema.clear();
		loadStyle = nullptr;
		operation = nullptr;
		needsReorg = false;
		reorging = false;

		// Setup script file
		deploy = fs.open("deploy.ps1").createOutputStream();

		// Define deploy script parameters, functions & initial processing
		*deploy << R"src(param([string]$bin, [string]$path, [string]$ini)

function jadclient {
	param([Parameter(ValueFromRemainingArguments)][string[]]$params)
	& $bin\jadclient path=$path ini=$ini server=remoteServer @params
	if ($lastexitcode) {
		exit $lastexitcode
	}
}

function jadload {
	param([Parameter(ValueFromRemainingArguments)][string[]]$params)
	& $bin\jadloadb path=$path ini=$ini server=remoteServer deleteIfAbsent=true suppressReorg=true @params
	if ($lastexitcode) {
		if ($lastexitcode -eq 8510) {
			Write-Warning "Load completed with methods in error"
		}
		elseif ($lastexitcode -eq 8511) {
			# Reorganisation required warning
		}
		else {
			exit $lastexitcode
		}
	}
}

function jadegit-deploy {
	param([Parameter(ValueFromRemainingArguments)][string[]]$params)
	& $bin\jadegit --path $path --ini $ini --multiUser deploy @params
	if ($lastexitcode) {
		exit $lastexitcode
	}
}

function check {
	param([string]$field, [string]$expected, [string]$actual)
	if ($actual -ne $expected) {
		Write-Warning "$field mismatch - expected: $expected, actual: $actual"
	}
}

function require {
	param([string]$field, [string]$required, [string]$actual)
	if ($actual -ne $required) {
		Write-Error "$field mismatch - required: $required, actual: $actual"
		exit 1
	}
}

$root = $PSScriptRoot

Write-Host "Deployment Started (bin: $bin, path: $path, ini: $ini)"
)src";
		
		// Handle checking manifest
		if (!origin.empty() || !currentCommit.empty())
		{
			changeOperation("Checking Manifest");
			*deploy << format("check \"origin\" \"{}\" (jadegit-deploy manifest get origin)", origin) << endl;
			*deploy << format("require \"commit\" \"{}\" (jadegit-deploy manifest get commit)", currentCommit) << endl;
		}	
	}

	void PowerShellDeploymentBuilder::finish(const string& origin, const string& commit)
	{
		if (!deploy)
			return;

		// Perform reorg if required
		if (needsReorg)
			Reorg();

		// Handle updating manifest
		if (!origin.empty() || !commit.empty())
		{
			changeOperation("Updating Manifest");
			*deploy << format("jadegit-deploy manifest set --commit \"{}\" --origin \"{}\"", commit, origin) << endl;
		}

		// Indicate end of deployment
		*deploy << endl << "Write-Host \"Deployment Finished\"" << endl << std::flush;
	}

	unique_ptr<ostream> PowerShellDeploymentBuilder::AddCommandFile(bool latestVersion)
	{
		// Change load style
		changeLoadStyle(latestVersion);

		return addFile("Commands", "jcf", "commandFile");
	}

	unique_ptr<ostream> PowerShellDeploymentBuilder::AddSchemaFile(const string& schema, bool latestVersion)
	{
		// Change load style
		changeLoadStyle(latestVersion);

		return addFile(schema, "scm", "schemaFile");
	}

	unique_ptr<ostream> PowerShellDeploymentBuilder::AddSchemaDataFile(const string& schema, bool latestVersion)
	{
		// Change load style
		changeLoadStyle(latestVersion);

		return addFile(schema, "ddx", "ddbFile");
	}

	void PowerShellDeploymentBuilder::addScript(const Build::Script& script)
	{
		// Handle pending re-org before running scripts
		if (needsReorg)
			Reorg();

		// Indicate change of operation
		changeOperation("Running Scripts");

		// Add script to deployment
		*deploy << format("jadclient schema={} app={}", script.schema, script.app);

		if (!script.executeSchema.empty())
			*deploy << format(" executeSchema={}", script.executeSchema);

		if (!script.executeClass.empty())
			*deploy << format(" executeClass={}", script.executeClass);

		if (!script.executeMethod.empty())
			*deploy << format(" executeMethod={}", script.executeMethod);

		if (!script.executeParam.empty())
			*deploy << format(" executeParam=\"{}\"", script.executeParam);

		if (script.executeTransient)
			*deploy << " executeTransient=true";

		if (script.executeTypeMethod)
			*deploy << " executeTypeMethod=true";
		
		*deploy << endl;
	}

	unique_ptr<ostream> PowerShellDeploymentBuilder::addFile(const string& schema, const char* extension, const string& param)
	{
		// Indicate change of operation
		changeOperation("Updating Schemas");

		// Determine if we can append subsequent ddbFile
		bool append = (param == "ddbFile" && schema == lastschema);
		
		// If not appending
		if (!append)
		{
			// Flush prior command
			flush();

			// Increment file counter
			files++;
		}
		
		// Derive filename
		stringstream filename;
		filename << "schemas\\" << setfill('0') << setw(2) << files << "-" << schema << "." << extension;

		// Open file
		File file = fs.open(filename.str());

		// Check file doesn't already exist
		if (file.exists())
			throw jadegit_exception("File already exists (" + filename.str() + ")");

		// Add to deployment script
		if (append)
			*deploy << format(" {}=\"$root\\{}\"", param, filename.str());
		else
			*deploy << format("jadload loadStyle={} {}=\"$root\\{}\"", loadStyle, param, filename.str());

		// Leave schemaFile load command open for appending subsequent ddbFile
		if (param == "schemaFile")
		{
			lastschema = schema;
		}
		else
		{
			lastschema.clear();
			*deploy << endl;
		}

		// Setup output stream
		return file.createOutputStream();
	}

	void PowerShellDeploymentBuilder::Reorg()
	{
		// Suppress duplicate re-orgs
		if (reorging)
			return;

		// Indicate change of operation
		changeOperation("Performing Reorganization");

		// Add to deployment script
		*deploy << "jadclient schema=RootSchema app=JadeReorgApp startAppParameters action=initiateReorgAllSchemas" << endl;

		// Set flag indicating we've just added re-org
		reorging = true;

		// Reset flag indicating reorg is needed
		needsReorg = false;
	}

	void PowerShellDeploymentBuilder::changeLoadStyle(bool latestVersion)
	{
		// Update load style
		loadStyle = latestVersion ? "latestSchemaVersion" : "currentSchemaVersion";

		// Reset flag indicating re-org has just been added
		reorging = false;

		// Set flag indicating reorg is needed
		if (latestVersion)
			needsReorg = true;

		// Handle pending re-org before loading into current schema version
		else if (needsReorg)
			Reorg();
	}

	void PowerShellDeploymentBuilder::changeOperation(const char* operation)
	{
		if (this->operation == operation)
			return;

		// Flush prior command
		flush();

		// Log change of operation
		*deploy << endl << format("Write-Host \"{}\"", operation) << endl;
		this->operation = operation;
	}

	void PowerShellDeploymentBuilder::flush()
	{
		if (!lastschema.empty())
		{
			lastschema.clear();
			*deploy << endl;
		}
	}
}
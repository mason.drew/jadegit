#pragma once
#include <build/Builder.h>

namespace JadeGit::Deploy
{
	class DeploymentBuilder : public Build::Builder
	{
	public:
		virtual void start(const std::string& origin, const std::string& currentCommit, const std::string& latestCommit) {};
		virtual void finish(const std::string& origin, const std::string& commit) {};
	};
}
#pragma once
#include "DeploymentBuilder.h"
#include <jadegit/vfs/FileSystem.h>

namespace JadeGit::Deploy
{
	class PowerShellDeploymentBuilder : public DeploymentBuilder
	{
	public:
		PowerShellDeploymentBuilder(const FileSystem& fs);

		void start(const std::string& origin, const std::string& currentCommit, const std::string& latestCommit) final;
		void finish(const std::string& origin, const std::string& commit) final;

		std::unique_ptr<std::ostream> AddCommandFile(bool latestVersion) final;
		std::unique_ptr<std::ostream> AddSchemaFile(const std::string& schema, bool latestVersion) final;
		std::unique_ptr<std::ostream> AddSchemaDataFile(const std::string& schema, bool latestVersion) final;

		void addScript(const Build::Script& script) final;

		void Reorg() final;

	protected:
		const FileSystem& fs;
		std::unique_ptr<std::ostream> deploy;
		int files = 0;
		std::string lastschema;
		const char* loadStyle = nullptr;
		const char* operation = nullptr;
		bool needsReorg = false;
		bool reorging = false;

		std::unique_ptr<std::ostream> addFile(const std::string& schema, const char* extension, const std::string& param);
		void changeLoadStyle(bool latestVersion);
		void changeOperation(const char* operation);
		void flush();
	};
}